<?php 
require_once ($CFG->dirroot.'/course/moodleform_mod.php');

class mod_gymkana_mod_form extends moodleform_mod {

    function definition() {

        $mform    =& $this->_form;
        
        $mform->addElement('text', 'name', get_string('name'), array('size' => '64'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', get_string('required'), 'required', null, 'client');

        
        $mform->addElement('htmleditor', 'intro', get_string('intro', 'gymkana'), array('size' => '64'));
        $mform->setType('intro', PARAM_RAW);
        $mform->addRule('intro', get_string('required'), 'required', null, 'client');
        
        
        $mform->addElement('text', 'timequest', get_string('timequest', 'gymkana'), array('size' => '1'));
        //$mform->setType('timequest', PARAM_INTEGER);
        $mform->setDefault('timequest', 180);
        $mform->addRule('timequest', get_string('required'), 'required', null, 'client');
        $mform->addRule('timequest', null, 'numeric', null, 'client');
        
        $mform->addElement('text', 'levels', get_string('numberlevels', 'gymkana'), array('size' => '1'));
        //$mform->setType('levels', PARAM_INTEGER);
        $mform->setDefault('levels', 5);
        $mform->addRule('levels', get_string('required'), 'required', null, 'client');
        $mform->addRule('levels', null, 'numeric', null, 'client');
        
        
        $mform->addElement('selectyesno', 'repeatquestions', get_string('allowrepeatquest', 'gymkana'));
        $mform->setDefault('repeatquestions', 0);
        
        $mform->addElement('selectyesno', 'allowstudentuplevel', get_string('allowstudentuplevel', 'gymkana'));
        $mform->setDefault('allowstudentuplevel', 1);
        
        $this->standard_coursemodule_elements();

        $this->add_action_buttons(true, false, null);

    }

}
?>
