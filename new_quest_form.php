<?php //$Id: new_quest_form.php,v 1.0 2012/04/10 20:46:32 rycis Exp $

require_once($CFG->dirroot.'/lib/formslib.php');


class new_quest_form extends moodleform {

    // Define the form
    function definition() {
        global $USER, $CFG, $COURSE;

        $mform =& $this->_form;

        $customdata =& $this->_customdata;
  

        //Agrego el campo para seleccionar el Nivel
        $options = array ();
        $options[null] = get_string('select_level', 'gymkana');

        for ($i=1;$i<=$customdata['maxlevels'];$i++){
            $options[$i] = $i;
        }

        
        $mform->addElement('select', 'level', get_string('level', 'gymkana'), $options);
        $mform->addRule('level', get_string('required'), 'required', null, 'client');
        
        $radiogrp = array();
        $radiogrp[] =& $mform->createElement('radio', 'action', null, get_string('write_answer', 'gymkana'), 'ans');
        $radiogrp[] =& $mform->createElement('radio', 'action', null, get_string('file2upload', 'gymkana'), 'ansfile');
        $mform->addGroup($radiogrp, 'actiongrp', get_string('choose_one', 'gymkana'), "<br/>", false);
        $mform->setDefault('action','ans'); 
        
        //Campos ocultos
        $mform->addElement('hidden', 'cmid', $customdata['moduleid']);
        
        $this->add_action_buttons(false, get_string('continue'));
    }

    function definition_after_data() {
        //global $USER, $CFG;

       // $mform =& $this->_form;
      
    }

    function validation($usernew, $files) {
        //global $CFG;
        
    }
}

class ans_form extends moodleform {

    // Define the form
    function definition() {
        global $USER, $CFG, $COURSE;
        $mform =& $this->_form;

        $customdata =& $this->_customdata;
  
        //quest
        $mform->addElement('text', 'quest', get_string('quest', 'gymkana'), 'size="100"');
        $mform->setType('quest', PARAM_RAW);
        $mform->addRule('quest', get_string('required'), 'required', null, 'client');
        
        //ans 
        if (!empty( $customdata['questid'] ) && !empty($customdata['file'] ) ) {

           $mform->addElement('hidden', 'ans', ''); 
           $mform->addElement('hidden', 'file', $customdata['file']); 
        } else {
            $mform->addElement('htmleditor', 'ans', get_string('answer', 'gymkana'));
            $mform->setType('ans', PARAM_RAW);
            $mform->addRule('ans', get_string('required'), 'required', null, 'client');
            
            $mform->addElement('hidden', 'file', ''); 
        }
        
        
         //help
        $mform->addElement('htmleditor', 'help', get_string('help', 'gymkana'));
        $mform->setType('help', PARAM_RAW);
        $mform->addRule('help', get_string('required'), 'required', null, 'client');
        
        //shortquest
        $mform->addElement('text', 'shortquest', get_string('shortquest', 'gymkana'), 'size="100"');
        $mform->setType('shortquest', PARAM_RAW);
        $mform->addRule('shortquest', get_string('required'), 'required', null, 'client');
        
        //shortquest
        $mform->addElement('htmleditor', 'shortans', get_string('shortanswer', 'gymkana'));
        $mform->setType('shortans', PARAM_RAW);
        $mform->addRule('shortans', get_string('required'), 'required', null, 'client');
        
        /// Agrego los campos ocultos necesarios
        $mform->addElement('hidden', 'level', $customdata['level']);
        $mform->setType('level', PARAM_INT);
        
        $mform->addElement('hidden', 'qid', $customdata['questid']);
        $mform->setType('qid', PARAM_INT);
        
        $mform->addElement('hidden', 'file', $customdata['file']);
        
        $mform->addElement('hidden', 'cmid', $customdata['moduleid']);
        $mform->addElement('hidden', 'action', 'ans');
         
        $this->add_action_buttons(false,get_string('new_quest', 'gymkana'));
    }

    function definition_after_data() {
        //global $USER, $CFG;

       // $mform =& $this->_form;
      
    }

    function validation($usernew, $files) {
        //global $CFG;
        
    }
}

class ansfile_form extends moodleform {

    // Define the form
    function definition() {
        global $USER, $CFG, $COURSE;

        $mform =& $this->_form;

        $customdata =& $this->_customdata;
  
        //Subir un fichero con la respuesta larga 
        $this->set_upload_manager(new upload_manager('ansfile', true, false, $COURSE, false, 0, true, true, false));
        $mform->addElement('file', 'ansfile', get_string('file2upload', 'gymkana'), 'size="40"');
        $mform->addRule('ansfile', null, 'required');
        
        
        /// Agrego los campos ocultos necesarios
        $mform->addElement('hidden', 'level', $customdata['level']);
        $mform->setType('level', PARAM_INT);
        
        $mform->addElement('hidden', 'cmid', $customdata['moduleid']);
        $mform->addElement('hidden', 'action', 'ansfile');
         
        $this->add_action_buttons(false, get_string('uploadafile'));
    }

    function definition_after_data() {
        //global $USER, $CFG;

       // $mform =& $this->_form;
      
    }

    function validation($usernew, $files) {
        //global $CFG;
        
    }
}


?>
