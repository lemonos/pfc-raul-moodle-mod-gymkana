<?php  // $Id: tabs.php,v 1.11 2007/03/16 04:10:20 mark-nielsen Exp $
/**
* Sets up the tabs used by the lesson pages for teachers.
*
* This file was adapted from the mod/quiz/tabs.php
*
* @version $Id: tabs.php,v 1.11 2007/03/16 04:10:20 mark-nielsen Exp $
* @license http://www.gnu.org/copyleft/gpl.html GNU Public License
* @package lesson
*/

/// This file to be included so we can assume config.php has already been included.

    if (empty($gymkana)) {
        error('You cannot call this script in that way');
    }
   
    if (!isset($currenttab)) {
        $currenttab = '';
    }
    if (!isset($cm)) {
        $cm = get_coursemodule_from_instance('gymkana', $gymkana->id);
        $context = get_context_instance(CONTEXT_MODULE, $cm->id);
    }
    if (!isset($course)) {
        $course = get_record('course', 'id', $gymkana->course);
    }
    
    $tabs = $row = $inactive = $activated = array();
    
    
    
    
    $row[] = new tabobject( 'view', 
                                "{$CFG->wwwroot}/mod/gymkana/view.php?a={$gymkana->id}",
                                get_string('modulename', 'gymkana'), 
                                get_string('modulename', 'gymkana')
                                );
    
    $row[] = new tabobject( 'userreports', 
                            "graficos_alumno.php?gymkananame{$gymkananame}'&course->id={$course->id}&gymkana->id={$gymkana->id}&course->shortname={$course->shortname}&cm->id={$cm->id}",
                            get_string('userreports', 'gymkana'), 
                            get_string('userre  ports', 'gymkana')
                            );
    
    
    if (has_capability('mod/gymkana:adminmod', $context)) {
        
        $row[] = new tabobject( 'quests', 
                                "{$CFG->wwwroot}/mod/gymkana/manage_quest.php?cmid={$cm->id}",
                                get_string('quests', 'gymkana'), 
                                get_string('quests', 'gymkana')
                                );
        
   /* }
    
    if (has_capability('mod/gymkana:resultadoalumnos', $context)) {    */
        $row[] = new tabobject( 'reports', 
                                "resultado.php?&cmid={$cm->id}",
                                get_string('reports', 'gymkana'), 
                                get_string('reports', 'gymkana')
                                );
    }
    $tabs[] = $row;
        
    switch ($currenttab) {
            case 'addquest':
            case 'managequest':
            /// sub tabs for reports (overview and detail)
                $inactive[] = 'quests';
                $activated[] = 'quests';

                $row    = array();
        
                $row[] = new tabobject( 'managequest', 
                                "manage_quest.php?cmid={$cm->id}",
                                get_string('managequest', 'gymkana'), 
                                get_string('managequest', 'gymkana')
                                
                                );
                $row[] = new tabobject( 'addquest', 
                                "{$CFG->wwwroot}/mod/gymkana/new_quest.php?cmid={$cm->id}",
                                get_string('addquest', 'gymkana'), 
                                get_string('addquest', 'gymkana')
                                );
        
                
                $tabs[] = $row;
                break;
            case 'graphics':
            case 'statistics':
            case 'results':
            /// sub tabs for edit view (collapsed and expanded aka full)
                $inactive[] = 'reports';
                $activated[] = 'reports';
                
                $row    = array();
                $row[] = new tabobject( 'results', 
                                "resultado.php?&cmid={$cm->id}",
                                get_string('results', 'gymkana'), 
                                get_string('results', 'gymkana')
                                );
                $row[] = new tabobject( 'graphics', 
                                "graficos.php?gymkananame={$gymkana->name}&course->id={$course->id}&gymkana->id={$gymkana->id}&course->shortname={$course->shortname}&cm->id={$cm->id}",
                                get_string('graphics', 'gymkana'), 
                                get_string('graphics', 'gymkana')
                                );
                $row[] = new tabobject( 'statistics', 
                                "estadisticas.php?gymkananame={$gymkana->name}&course->id={$course->id}&gymkana->id={$gymkana->id}&course->shortname={$course->shortname}&cm->id={$cm->id}",
                                get_string('statistics', 'gymkana'), 
                                get_string('statistics', 'gymkana')
                                );
                $tabs[] = $row;
                break;
            
            case 'usergraphics':
            case 'userstatistics':
            /// sub tabs for edit view (collapsed and expanded aka full)
                $inactive[] = 'userreports';
                $activated[] = 'userreports';
                
                $row    = array();
                $row[] = new tabobject( 'usergraphics', 
                                "graficos_alumno.php?gymkananame{$gymkananame}'&course->id={$course->id}&gymkana->id={$gymkana->id}&course->shortname={$course->shortname}&cm->id={$cm->id}",
                                get_string('graphics', 'gymkana'), 
                                get_string('graphics', 'gymkana')
                                );
                $row[] = new tabobject( 'userstatistics', 
                                "estadisticas_alumno.php?gymkananame={$gymkananame}&course->id={$course->id}&gymkana->id={$gymkana->id}&course->shortname={$course->shortname}&cm->id={$cm->id}",
                                get_string('statistics', 'gymkana'), 
                                get_string('statistics', 'gymkana')
                                );
                $tabs[] = $row;
                break;
        }
        
    

    


    /**/

    print_tabs($tabs, $currenttab, $inactive, $activated);
    
    print_heading(format_string($gymkana->name));

?>
