<?php

	require_once("../../config.php");
	require_once("lib.php");
	

	$gymkananame = optional_param('gymkananame', 0, PARAM_CLEAN); 
    $course->id  = optional_param('course->id', 0, PARAM_INT);  
	$gymkana->id  = optional_param('gymkana->id', 0, PARAM_INT);  
	$course->shortname = optional_param('course->shortname', 0, PARAM_CLEAN);
	$cm->id = optional_param('cm->id', 0, PARAM_INT);
    $a  = optional_param('a', 0, PARAM_INT);  // gymkana ID

    if ($cm->id) {
        if (! $cm = get_record("course_modules", "id", $cm->id)) {
            error("Course Module ID was incorrect");
        }
		if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
   
    } 

	
	$action = optional_param('action', 'opcionesbusqueda', PARAM_ALPHA);
	$buscar = optional_param('buscar', 'nada', PARAM_ALPHA);
	$mostrar = optional_param('mostrar', 'nada', PARAM_ALPHA);
	$usuario = optional_param('usuario', 'opcionesbusqueda', PARAM_ALPHA);
	$nivelseleccionado = optional_param('nivelseleccionado', 'opcionesbusqueda', PARAM_INT);
	$dia = optional_param('dia',PARAM_INT);
	$mes = optional_param('mes',PARAM_INT);
	$anio = optional_param('anio',PARAM_INT);
	$dia2 = optional_param('dia2',PARAM_INT);
	$mes2 = optional_param('mes2',PARAM_INT);
	$anio2 = optional_param('anio2',PARAM_INT);
	$hora = optional_param('hora',PARAM_INT);
	$minutos = optional_param('minutos',PARAM_INT);
	$meses[1]= 'Enero';
    $meses[2]= 'Febrero';
    $meses[3]= 'Marzo';
    $meses[4]= 'Abril';
    $meses[5]= 'Mayo';
    $meses[6]= 'Junio';
    $meses[7]= 'Julio';
    $meses[8]= 'Agosto';
    $meses[9]= 'Septiembre';
    $meses[10]= 'Octubre';
    $meses[11]= 'Noviembre';
    $meses[12]= 'Diciembre';
	$fechaseleccionada = optional_param('fechaseleccionada', PARAM_ALPHA);
	$fechaseleccionada2 = optional_param('fechaseleccionada2', PARAM_ALPHA);
	$horainicio = optional_param('fechaseleccionada', PARAM_ALPHA);
	$grafico = optional_param('grafico', 'nada', PARAM_ALPHA);
	$suma = optional_param('suma', '0', PARAM_INT);
	$desviacion = optional_param('desviacion', 0, PARAM_INT);
	$media = optional_param('media', 0, PARAM_INT);
	$Q1 = optional_param('Q1', 0, PARAM_INT);
	$Q2 = optional_param('Q2', 0, PARAM_INT);
	$Q3 = optional_param('Q3', 0, PARAM_INT);
	$min = optional_param('min', 0, PARAM_INT);
	$max = optional_param('max', 0, PARAM_INT);
	$anterior= optional_param('anterior', 0, PARAM_INT);
	$io = optional_param('io', 0, PARAM_INT);
	$ultimo = optional_param('ultimo', 0, PARAM_INT);
	$num_results3 = optional_param('num_results3', 0, PARAM_INT);
	
  require_course_login($course);
  $context = get_context_instance(CONTEXT_MODULE, $cm->id);

    add_to_log($course->id, "gymkana", "graficos", "graficos_alumno.php?id=".$cm->id, $gymkana->id);
	
/// Print the page header

    $strgymkanas = get_string("modulenameplural", "gymkana");
    $strgymkana  = get_string("modulename", "gymkana");

	$navigation1 = "<a href=\"index.php?cm=$cm->id\">$strgymkanas</a> ->";			  
	$navigation2 = "<a href=\"view.php?a=$gymkana->id\">".stripslashes(format_string($gymkana->name))."</a> -> ";
	print_header_simple($gymkananame." - Resultados graficos", "",
                 "$navigation1 "."$navigation2 "."Mis Resultados graficos");

		@ $username = $HTTP_POST_VARS['username'];
		$username = addslashes($username);
		
		$username = $USER -> username;
        
        //INTRODUZCO PESTA�AS
    $currenttab = 'usergraphics';
    include ('tabs.php'); 

//Funcion para seleccionar el metodo de busqueda   
?>
	<script language="Javascript" type="text/javascript">
	
		function jumpPage(newLoc) 
		{
			newPage = newLoc.options[newLoc.selectedIndex].value
			
			if (newPage != "") 
			{
				window.location = newPage
			}
		}
	</script>      
<?php

echo '<center>';
echo '<caption><h2><u>Mis Resultados gr&aacute;ficos</u></h2></caption>';
		echo '<br>';
		
		
	
		//Selector de grafica
		echo '<select name="newLocation" onchange="jumpPage(this)">';
			echo '<option>Seleccione el tipo de gr&aacute;fica a mostrar</option>';
			echo '<option value="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action='.$action.'&grafico=nada&user='.$username.'">Ninguno</option>';
			echo '<option value="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action='.$action.'&grafico=campana&user='.$username.'">Campana</option>';
			echo '<option value="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action='.$action.'&grafico=error&user='.$username.'">Gr&aacute;fico error</option>';
			echo '<option value="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action='.$action.'&grafico=lista&user='.$username.'">Gr&aacute;fico lista</option>';
		echo '</select>';
		echo '&nbsp;&nbsp;&nbsp;&nbsp;';		

//Selector de Metodo de busqueda
		echo '<select name="newLocation" onchange="jumpPage(this)">';
			echo '<option>Seleccione un m&eacute;todo de b&uacute;squeda</option>';
			echo '<option value="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action=todos&grafico='.$grafico.'&user='.$username.'">Todos los resultados</option>';
			echo '<option value="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action=fecha&grafico='.$grafico.'&fechaseleccionada'.$fechaseleccionada.'&user='.$username.'">A partir de una fecha</option>';
			echo '<option value="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action=intervalofecha&grafico='.$grafico.'&fechaseleccionada'.$fechaseleccionada.'&fechaseleccionada2'.$fechaseleccionada2.'&user='.$username.'">En un intervalo de fechas</option>';
		echo '</select>';
		echo '<br><br>';
		
		
echo '</center>';
echo '<br/>';

// MUESTRA TODOS LOS RESULTADOS
if ($action == 'todos') {
    print_graph_data_table("SELECT * FROM ".$CFG->prefix."gymkana_games WHERE gymkana='$gymkana->id' AND user='$USER->id' ORDER BY id");
	
	if ($grafico == 'campana' || $grafico == 'error' || $grafico == 'lista' || $mostrar == 'ok') {
	
	$query = "SELECT AVG(score) FROM ".$CFG->prefix."gymkana_games WHERE gymkana='$gymkana->id' AND user='$USER->id'";
			$results = mysql_query($query);
			$num_results = mysql_num_rows($results);
			if ($num_results > 0) {
				$row = mysql_fetch_row($results); 
				$media = $row[0];
				$media=$media;
				//redondeamos la media a 4 decimales
				$media = (round($media * 10000) / 10000);
				
			}
			
		if ($grafico == 'campana') {
		//consulta que halla la media de la columna de puntuaciones total

			$query2 = "SELECT SUM(score) FROM ".$CFG->prefix."gymkana_games WHERE gymkana='$gymkana->id' AND user='$USER->id'";
			$results2 = mysql_query($query2);
			$num_results2 = mysql_num_rows($results2);
			if ($num_results2 > 0) {
				$row2 = mysql_fetch_row($results2); 
				$suma = $row2[0];
				$primertermino = $suma;
				$segundotermino = $media*$media;
				$varianza = ($primertermino - $segundotermino)/$num_results2;
				//redondeamos la varianza a 4 decimales
				$varianza = abs( (round($varianza * 10000) / 10000) ); 
				$desviacion = sqrt($varianza);
				//redondeamos la desviacion a 4 decimales
				$desviacion = (round($desviacion * 10000) / 10000);	
			}
	
		if ($desviacion != 0) {
			$media = $media * 10000; //Se pasan 4 decimales
			$desviacion = $desviacion * 10000; //Se pasan 4 decimales
						
			echo "<br><center><img src='$CFG->wwwroot/mod/gymkana/grafico_media.php?gymkananame=$gymkananame&course->id=$course->id&gymkana->id=$gymkana->id&course->shortname=$course->shortname&cm->id=$cm->id&media=$media&desviacion=$desviacion&action=$action&grafico=campana&usuario=usuario&nivelseleccionado=$nivelseleccionado'/></center><br><br><br>";
		}
		else
			{
			echo '<center>';
			echo '<div class="errorbox">';
			echo 'Esta grafica no tiene sentido si la desviacion es 0';
			echo '</div>';
			echo '<br></center>';  
		}
	} //fin grafico campana

		//Grafico error
		if ($grafico == 'error') {
			//consulta que halla la media de la columna de puntuaciones total
			$query2 = "SELECT score FROM ".$CFG->prefix."gymkana_games WHERE gymkana='$gymkana->id' AND user='$USER->id' ORDER BY score";
			$results2 = mysql_query($query2);
			$num_results2 = mysql_num_rows($results2);

			$aux = $num_results2/2;
				$aux = floor($aux);
			$aux2 = $num_results2/(4);
				$aux2 = floor($aux2);
			$aux3 = $num_results2*(3/4);
				$aux3 = floor($aux3);
			$aux4 = $num_results2/4;
				$aux4 = floor($aux4);
			$aux5 = $num_results2*(3/4);
				$aux5 = floor($aux5);
		
			
			for ($i=0; $i<$num_results2; $i++){			
				$row2 = mysql_fetch_array ($results2);		
				if ($i==0){
					$min = $row2['score'];
					}
				if ($i==($num_results2-1)){
					$max = $row2['score'];
					}
				if ($i==($aux-1)){
					$menos = $row2['score'];
					}
				if ($i==($aux)){
					$igual = $row2['score'];
					}
				if ($i==($aux+1)){
					$mas = $row2['score'];
					}
				if ($i==($aux2)){
					$igual2 = $row2['score'];
					}
				if ($i==($aux3)){
					$igual3 = $row2['score'];					
					}
				if ($i==($aux4-1)){
					$menos4 = $row2['score'];
					}
				if ($i==($aux4)){
					$igual4 = $row2['score'];
					}
				if ($i==($aux5-1)){
					$menos5 = $row2['score'];
					}
				if ($i==($aux5)){
					$igual5 = $row2['score'];
					}
				if ($i==($aux5+1)){
					$mas5 = $row2['score'];
					}		
			}
			
			if ($num_results2 > 3) { // Solo muestra la grafica si hay mas de 3 resultados
		        if($num_results2 % 2 == 0) //numero par de notas
		        {
			        $valor = $num_results2/2;
			        $Q2 = ($igual+$menos)/2;
			        if($valor % 2 != 0) //numero impar
			    	{
			       		$Q1=$igual2;
			       		$Q3=$igual3;
			       	}
			       	else //numero par
			       	{
			      		$Q1 = ($igual4+$menos4)/2;
			      		$Q3 = ($igual5+$menos5)/2;	
		       	  	}
			    }
			    else //numero impar de notas
		        {
			    	$valor = $num_results2/2; 
			    	$valor = floor($valor); //obtiene parte entera
			    	$Q2=$igual;
			    	if($valor % 2 != 0)
			    	{
			       		$Q1=$igual2;
			       		$Q3=$igual3;
			       	}
			       	else
			       	{
			      		$Q1 = ($igual4+$menos4)/2;
			      		$Q3 = ($igual5+$mas5)/2;		       	
		       	  	}	
				} 
	
				$Q1 = round($Q1 * 100); //redondeo 2 decimales y multiplico por 100 para pasarlo como entero
				$Q2 = round($Q2 * 100);
				$Q3 = round($Q3 * 100);	

				$min = $min*100; //multiplico por 100 para pasarlo como entero
				$max = $max*100;
				$media = $media*100;
				$usuario = 'Todos';

			    echo "<center><br><img src='$CFG->wwwroot/mod/gymkana/grafico_error.php?gymkananame=$gymkananame&course->id=$course->id&gymkana->id=$gymkana->id&course->shortname=$course->shortname&cm->id=$cm->id&Q1=$Q1&Q2=$Q2&Q3=$Q3&max=$max&min=$min&media=$media&action=$action&grafico=$error&usuario=$usuario&nivelseleccionado=$nivelseleccionado' /><br><br><br></center>";
			}
	        else
	        {
				echo '<center>';
				echo '<div class="errorbox">';
				echo 'Error, Esta grafica solo tiene sentido con m&aacute;s de 3 resultados';
				echo '</div>';
				echo '<br></center>';  
	    	}

}//fin grafico error


/// Gr�fico lista		
if ($grafico == 'lista') {

			$query3 = "SELECT user, score FROM ".$CFG->prefix."gymkana_games WHERE gymkana='$gymkana->id' AND user='$USER->id'";
			$results3 = mysql_query($query3);
			$num_results3 = mysql_num_rows($results3);
			
			//print_object($punt);
			//echo $numnotas.'<br>';
			$numnotas= ceil($num_results3/10); //redondea hacia arriba
			$anterior=1;
			echo '<br><b>Mostrar resultados del : </b>';
			for($i=1;$i<$numnotas+1;$i++)
			{
				$io=10*$i;
				if($numnotas!=$i)
				{
					echo '<font color=black><a href="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action='.$action.'&grafico=lista&mostrar=ok&primero='.$anterior.'&io='.$io.'&ultimo='.$ultimo.'&user='.$username.'"><b>'.$anterior.' al'.' '.$io.', </a><b></font>';
				}
				else
				{
					echo '<font color=black><a href="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action='.$action.'&grafico=lista&mostrar=ok&ultimo=1&primero='.$anterior.'&io='.$io.'&user='.$username.'"><b>'.$anterior.' al '.$num_results3.'. </a><b></font>';
				}
				$anterior=$io+1;
			}
		
			//$numero=1;
			for($a=2;$a<12;$a++){
				
				$punt=mysql_fetch_array($results3);
				$punt[$a]->puntuacion = $punt['score'];
				$punt[$a]->usuario = $punt['user'];
				
				//introduce 10 valores en datos, si no hay valor mete un cero
					//echo $numero.'<br>';
					if($punt[$a]->usuario == '')
					{
					 	$datos[$a]->usuario = ' ';
					 	$datos[$a]->puntuacion = 0;
				 	}
				 	else
				 	{
						$datos[$a]->usuario= $punt[$a]->usuario;
						$datos[$a]->puntuacion= $punt[$a]->puntuacion;
					}
				}
				echo'<br><br>';
				
			}
			
			if ($mostrar == 'ok'){
			
				$max=10;
				$min=0;
				/*for($a=1;$a<11;$a++) //calculo del max y min
				{
					if($datos[$a]->puntuacion>$max)
					{
						$max = $datos[$a]->puntuacion;
					}
					if($datos[$i]->puntuacion<$min)
					{
						$min = $datos[$a]->puntuacion;
					}
				echo 'MAX:'.$max.'MIN:'.$min.'<br>';
				}*/
				
				$nombre1=$datos[2]->usuario;
				$nombre2=$datos[3]->usuario;
				$nombre3=$datos[4]->usuario;
				$nombre4=$datos[5]->usuario;
				$nombre5=$datos[6]->usuario;
				$nombre6=$datos[7]->usuario;
				$nombre7=$datos[8]->usuario;
				$nombre8=$datos[9]->usuario;
				$nombre9=$datos[10]->usuario;
				$nombre10=$datos[11]->usuario;
				
						 
				$nota1=$datos[2]->puntuacion*100; //paso la nota como entero (tiene 2 decimales)
				$nota2=$datos[3]->puntuacion*100;
				$nota3=$datos[4]->puntuacion*100;
				$nota4=$datos[5]->puntuacion*100;
				$nota5=$datos[6]->puntuacion*100;
				$nota6=$datos[7]->puntuacion*100;
				$nota7=$datos[8]->puntuacion*100;
				$nota8=$datos[9]->puntuacion*100;
				$nota9=$datos[10]->puntuacion*100;
				$nota10=$datos[11]->puntuacion*100;
						
				$max = $max * 100;
				$min = $min * 100;
				
							
				//echo 'MAX:'.$max.'MIN:'.$min.'<br>';
											
				echo "<center><br><img src='$CFG->wwwroot/mod/gymkana/grafico_lista.php?gymkananame=$gymkananame&course->id=$course->id&gymkana->id=$gymkana->id&course->shortname=$course->shortname&cm->id=$cm->id&action=$action&grafico=lista&mostrar=ok&usuario=$usuario&nivelseleccionado=$nivelseleccionado&nam1=$nombre1&nam2=$nombre2&nam3=$nombre3&nam4=$nombre4&nam5=$nombre5&nam6=$nombre6&nam7=$nombre7&nam8=$nombre8&nam9=$nombre9&nam10=$nombre10&not1=$nota1&not2=$nota2&not3=$nota3&not4=$nota4&not5=$nota5&not6=$nota6&not7=$nota7&not8=$nota8&not9=$nota9&not10=$nota10&max=$max&min=$min&numero=$numero&numnotas=$numnotas&ultimo=$ultimo' /><br><br><br></center>";
			}
			

		 // Fin gr�fico lista
		
		
		
			
		} //fin graficos
}

 
 
// MUESTRA UN BUSCADOR DE FECHA --> a partir de una fecha

if ($action =='fecha') 
		{

$buscar == 'fecha';
echo '<form method="post" action="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action='.$action.'&fechaseleccionada='.$fechaseleccionada.'&buscar=fecha&grafico='.$grafico.'&user='.$username.'" name="fecha">';	
//print_simple_box_start ("center");
echo '<center>&nbsp;&nbsp;A partir de la fecha&nbsp;&nbsp;   ';
		echo '<select name="dia">';
			echo '<option selected=" "> </option>';
			echo '<option value="1">1</option>';
			echo '<option value="2">2</option>';
			echo '<option value="3">3</option>';
			echo '<option value="4">4</option>';
			echo '<option value="5">5</option>';
			echo '<option value="6">6</option>';
			echo '<option value="7">7</option>';
			echo '<option value="8">8</option>';
			echo '<option value="9">9</option>';
			echo '<option value="10">10</option>';
			echo '<option value="11">11</option>';
			echo '<option value="12">12</option>';
			echo '<option value="13">13</option>';
			echo '<option value="14">14</option>';
			echo '<option value="15">15</option>';
			echo '<option value="16">16</option>';
			echo '<option value="17">17</option>';
			echo '<option value="18">18</option>';
			echo '<option value="19">19</option>';
			echo '<option value="20">20</option>';
			echo '<option value="21">21</option>';
			echo '<option value="22">22</option>';
			echo '<option value="23">23</option>';
			echo '<option value="24">24</option>';
			echo '<option value="25">25</option>';
			echo '<option value="26">26</option>';
			echo '<option value="27">27</option>';
			echo '<option value="28">28</option>';
			echo '<option value="29">29</option>';
			echo '<option value="30">30</option>';
			echo '<option value="31">31</option>';
		echo '</select>';
		echo '&nbsp;';
		echo '<select name="mes">';
			echo '<option selected=" "> </option>';
			echo '<option value="1">Enero</option>';
			echo '<option value="2">Febrero</option>';
			echo '<option value="3">Marzo</option>';
			echo '<option value="4">Abril</option>';
			echo '<option value="5">Mayo</option>';
			echo '<option value="6">Junio</option>';
			echo '<option value="7">Julio</option>';
			echo '<option value="8">Agosto</option>';
			echo '<option value="9">Septiembre</option>';
			echo '<option value="10">Octubre</option>';
			echo '<option value="11">Noviembre</option>';
			echo '<option value="12">Diciembre</option>';
		echo '</select>';
		echo '&nbsp;';
		echo '<select name="anio">';
		echo '<option selected=" "> </option>';
			echo '<option value="2009">2009</option>';
			echo '<option value="2010">2010</option>';
			echo '<option value="2011">2011</option>';
			echo '<option value="2012">2012</option>';
			echo '<option value="2013">2013</option>';
			echo '<option value="2014">2014</option>';
			echo '<option value="2015">2015</option>';
			echo '<option value="2016">2016</option>';
			echo '<option value="2017">2017</option>';
			echo '<option value="2018">2018</option>';
	        echo '<option value="2019">2019</option>';
			echo '<option value="2020">2020</option>';
			echo '<option value="2021">2021</option>';
			echo '<option value="2022">2022</option>';
		echo '</select>';      
				
		echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';          
	    	echo '<input type=\'submit\' value="Buscar"></center>';
	     	echo '</form>';
	    	
			
		
		
		// Funci�n que comprueba la fecha introducida
		
		if (!checkdate($mes, $dia, $anio)) {	      	
			
			echo '<center>';
				echo '<div class="errorbox">';
				echo 'La fecha introducida no es correcta, por favor introd&uacute;zcala de nuevo';
				echo '</div>';
			echo '<br></center>'; 
			
						
		}
		
		$fechaseleccionada=$anio.'-'.$mes.'-'.$dia;
		if ($fechaseleccionada == '2-2-2') {
		}
		else {
		$fechaseleccionada = strtotime($fechaseleccionada);
		if ($grafico == 'campana' || $grafico == 'error' || $grafico == 'lista' || $mostrar == 'ok' || $buscar == 'fecha') {
	if ($buscar == 'fecha') {
	
			echo "<h3><center>Fecha seleccionada: ".$fechaseleccionada."</center></h3><br />";	
            print_graph_data_table("SELECT * FROM ".$CFG->prefix."gymkana_games WHERE date>'$fechaseleccionada' AND gymkana='$gymkana->id' AND user='$USER->id'");	

	} 
	
		$query = "SELECT AVG(score) FROM ".$CFG->prefix."gymkana_games WHERE date>'$fechaseleccionada' AND gymkana='$gymkana->id' AND user='$USER->id'";
		$results = mysql_query($query);
		$num_results = mysql_num_rows($results);
		if ($num_results > 0) {
			$row = mysql_fetch_row($results); 
			$media = $row[0];
			$media=$media;
			//redondeamos la media a 4 decimales
			$media = (round($media * 10000) / 10000); 	
		}
		
	if ($grafico == 'campana') {

	//consulta que halla la media de la columna de puntuaciones total
		
		
		$query2 = "SELECT SUM(score) FROM ".$CFG->prefix."gymkana_games WHERE date>'$fechaseleccionada' AND gymkana='$gymkana->id' AND user='$USER->id'";
		$results2 = mysql_query($query2);
		$num_results2 = mysql_num_rows($results2);
		if ($num_results2 > 0) {
			$row2 = mysql_fetch_row($results2); 
			$suma = $row2[0];
			$primertermino = $suma;
			$segundotermino = $media*$media;
			$varianza = ($primertermino - $segundotermino)/$num_results2;
					//redondeamos la varianza a 4 decimales
			$varianza = (round($varianza * 10000) / 10000); 
				
			$desviacion = sqrt($varianza);
			//redondeamos la desviacion a 4 decimales
			$desviacion = (round($desviacion * 10000) / 10000);	
		}
	if ($desviacion != 0) {
		$media = $media * 10000; //Se pasan 4 decimales
		$desviacion = $desviacion * 10000; //Se pasan 4 decimales
					
		echo "<br><center><img src='$CFG->wwwroot/mod/gymkana/grafico_media.php?gymkananame=$gymkananame&course->id=$course->id&gymkana->id=$gymkana->id&course->shortname=$course->shortname&cm->id=$cm->id&media=$media&desviacion=$desviacion&action=$action&grafico=campana&usuario=usuario&buscar=fecha&fechaseleccionada=$fechaseleccionada&mes=$mes&dia=$dia&anio=$anio'/></center><br><br><br>"; 

	}
	else {
		echo '<center>';
		echo '<div class="errorbox">';
		echo 'Esta grafica no tiene sentido si la desviacion es 0';
		echo '</div>';
		echo '<br></center>';
		 
	}
			} //Fin grafico campana
			
			//Grafico error
		if ($grafico == 'error') {
			//consulta que halla la media de la columna de puntuaciones total
			$query2 = "SELECT score FROM ".$CFG->prefix."gymkana_games WHERE date>'$fechaseleccionada' AND gymkana='$gymkana->id' AND user='$USER->id' ORDER BY score";
			$results2 = mysql_query($query2);
			$num_results2 = mysql_num_rows($results2);

			$aux = $num_results2/2;
				$aux = floor($aux);
			$aux2 = $num_results2/(4);
				$aux2 = floor($aux2);
			$aux3 = $num_results2*(3/4);
				$aux3 = floor($aux3);
			$aux4 = $num_results2/4;
				$aux4 = floor($aux4);
			$aux5 = $num_results2*(3/4);
				$aux5 = floor($aux5);
		
			
			for ($i=0; $i<$num_results2; $i++){			
				$row2 = mysql_fetch_array ($results2);		
				if ($i==0){
					$min = $row2['score'];
					}
				if ($i==($num_results2-1)){
					$max = $row2['score'];
					}
				if ($i==($aux-1)){
					$menos = $row2['score'];
					}
				if ($i==($aux)){
					$igual = $row2['score'];
					}
				if ($i==($aux+1)){
					$mas = $row2['score'];
					}
				if ($i==($aux2)){
					$igual2 = $row2['score'];
					}
				if ($i==($aux3)){
					$igual3 = $row2['score'];					
					}
				if ($i==($aux4-1)){
					$menos4 = $row2['score'];
					}
				if ($i==($aux4)){
					$igual4 = $row2['score'];
					}
				if ($i==($aux5-1)){
					$menos5 = $row2['score'];
					}
				if ($i==($aux5)){
					$igual5 = $row2['score'];
					}
				if ($i==($aux5+1)){
					$mas5 = $row2['score'];
					}		
			}
			
			if ($num_results2 > 3) { // Solo muestra la grafica si hay mas de 3 resultados
		        if($num_results2 % 2 == 0) //numero par de notas
		        {
			        $valor = $num_results2/2;
			        $Q2 = ($igual+$menos)/2;
			        if($valor % 2 != 0) //numero impar
			    	{
			       		$Q1=$igual2;
			       		$Q3=$igual3;
			       	}
			       	else //numero par
			       	{
			      		$Q1 = ($igual4+$menos4)/2;
			      		$Q3 = ($igual5+$menos5)/2;	
		       	  	}
			    }
			    else //numero impar de notas
		        {
			    	$valor = $num_results2/2; 
			    	$valor = floor($valor); //obtiene parte entera
			    	$Q2=$igual;
			    	if($valor % 2 != 0)
			    	{
			       		$Q1=$igual2;
			       		$Q3=$igual3;
			       	}
			       	else
			       	{
			      		$Q1 = ($igual4+$menos4)/2;
			      		$Q3 = ($igual5+$mas5)/2;		       	
		       	  	}	
				} 
	
				$Q1 = round($Q1 * 100); //redondeo 2 decimales y multiplico por 100 para pasarlo como entero
				$Q2 = round($Q2 * 100);
				$Q3 = round($Q3 * 100);	

				$min = $min*100; //multiplico por 100 para pasarlo como entero
				$max = $max*100;
				$media = $media*100;

			    echo "<center><br><img src='$CFG->wwwroot/mod/gymkana/grafico_error.php?gymkananame=$gymkananame&course->id=$course->id&gymkana->id=$gymkana->id&course->shortname=$course->shortname&cm->id=$cm->id&Q1=$Q1&Q2=$Q2&Q3=$Q3&max=$max&min=$min&media=$media&action=$action&grafico=$error&usuario=Todos&fechaseleccionada=$fechaseleccionada&mes=$mes&dia=$dia&anio=$anio' /><br><br><br></center>";
			}
	        else
	        {
				echo '<center>';
				echo '<div class="errorbox">';
				echo 'Error, Esta grafica solo tiene sentido con m&aacute;s de 3 resultados';
				echo '</div>';
				echo '<br></center>';  
	    	}

}//fin grafico error


/// Gr�fico lista		
if ($grafico == 'lista') {

			$query3 = "SELECT user, score FROM ".$CFG->prefix."gymkana_games WHERE date>'$fechaseleccionada' AND gymkana='$gymkanana->id' AND user='$USER->id'";
			$results3 = mysql_query($query3);
			$num_results3 = mysql_num_rows($results3);
			
			//print_object($punt);
			//echo $numnotas.'<br>';
			$numnotas= ceil($num_results3/10); //redondea hacia arriba
			$anterior=1;
			echo '<br><b>Mostrar resultados del : </b>';
			for($i=1;$i<$numnotas+1;$i++)
			{
				$io=10*$i;
				if($numnotas!=$i)
				{
					echo '<font color=black><a href="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action=fecha&grafico=lista&mostrar=ok&primero='.$anterior.'&io='.$io.'&ultimo='.$ultimo.'&mes='.$mes.'&dia='.$dia.'&anio='.$anio.'&fechaseleccionada='.$fechaseleccionada.'&buscar=fecha&user='.$username.'"><b>'.$anterior.' al'.' '.$io.', </a><b></font>';
				}
				else
				{
					echo '<font color=black><a href="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action=fecha&grafico=lista&mostrar=ok&ultimo=1&primero='.$anterior.'&io='.$io.'&mes='.$mes.'&dia='.$dia.'&anio='.$anio.'&fechaseleccionada='.$fechaseleccionada.'&buscar=fecha&user='.$username.'"><b>'.$anterior.' al '.$num_results3.'. </a><b></font>';
				}
				$anterior=$io+1;
			}
		
			//$numero=1;
			for($a=2;$a<12;$a++){
				
				$punt=mysql_fetch_array($results3);
				$punt[$a]->puntuacion = $punt['score'];
				$punt[$a]->usuario = $punt['user'];
				
				//introduce 10 valores en datos, si no hay valor mete un cero
					//echo $numero.'<br>';
					if($punt[$a]->usuario == '')
					{
					 	$datos[$a]->usuario = ' ';
					 	$datos[$a]->puntuacion = 0;
				 	}
				 	else
				 	{
						$datos[$a]->usuario= $punt[$a]->usuario;
						$datos[$a]->puntuacion= $punt[$a]->puntuacion;
					}
				}
				echo'<br><br>';
				
			}
			
			if ($mostrar == 'ok'){
			
				$max=10;
				$min=0;
								
				$nombre1=$datos[2]->usuario;
				$nombre2=$datos[3]->usuario;
				$nombre3=$datos[4]->usuario;
				$nombre4=$datos[5]->usuario;
				$nombre5=$datos[6]->usuario;
				$nombre6=$datos[7]->usuario;
				$nombre7=$datos[8]->usuario;
				$nombre8=$datos[9]->usuario;
				$nombre9=$datos[10]->usuario;
				$nombre10=$datos[11]->usuario;
				
						 
				$nota1=$datos[2]->puntuacion*100; //paso la nota como entero (tiene 2 decimales)
				$nota2=$datos[3]->puntuacion*100;
				$nota3=$datos[4]->puntuacion*100;
				$nota4=$datos[5]->puntuacion*100;
				$nota5=$datos[6]->puntuacion*100;
				$nota6=$datos[7]->puntuacion*100;
				$nota7=$datos[8]->puntuacion*100;
				$nota8=$datos[9]->puntuacion*100;
				$nota9=$datos[10]->puntuacion*100;
				$nota10=$datos[11]->puntuacion*100;
						
				$max = $max * 100;
				$min = $min * 100;
				
							
				//echo 'MAX:'.$max.'MIN:'.$min.'<br>';
											
				echo "<center><br><img src='$CFG->wwwroot/mod/gymkana/grafico_lista.php?gymkananame=$gymkananame&course->id=$course->id&gymkana->id=$gymkana->id&course->shortname=$course->shortname&cm->id=$cm->id&action=$action&grafico=lista&mostrar=ok&usuario=$usuario&nivelseleccionado=$nivelseleccionado&nam1=$nombre1&nam2=$nombre2&nam3=$nombre3&nam4=$nombre4&nam5=$nombre5&nam6=$nombre6&nam7=$nombre7&nam8=$nombre8&nam9=$nombre9&nam10=$nombre10&not1=$nota1&not2=$nota2&not3=$nota3&not4=$nota4&not5=$nota5&not6=$nota6&not7=$nota7&not8=$nota8&not9=$nota9&not10=$nota10&max=$max&min=$min&numero=$numero&numnotas=$numnotas&ultimo=$ultimo' /><br><br><br></center>";
			
			} // Fin gr�fico lista
			} //Fin gr�ficos
			

		}
}


// MUESTRA DOS BUSCADORES DE FECHA --> en un intervalo de fechas

if ($action =='intervalofecha') 
		{
			$buscar = 'intervalofecha';

echo '<form method="post" action="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action='.$action.'&grafico='.$grafico.'&buscar=intervalofecha&user='.$username.'" name="intervalofecha">';	
//print_simple_box_start ("center");
echo '<center>&nbsp;&nbsp;A partir de la fecha&nbsp;&nbsp;   ';
		echo '<select name="dia">';
			echo '<option selected=" "> </option>';
			echo '<option value="1">1</option>';
			echo '<option value="2">2</option>';
			echo '<option value="3">3</option>';
			echo '<option value="4">4</option>';
			echo '<option value="5">5</option>';
			echo '<option value="6">6</option>';
			echo '<option value="7">7</option>';
			echo '<option value="8">8</option>';
			echo '<option value="9">9</option>';
			echo '<option value="10">10</option>';
			echo '<option value="11">11</option>';
			echo '<option value="12">12</option>';
			echo '<option value="13">13</option>';
			echo '<option value="14">14</option>';
			echo '<option value="15">15</option>';
			echo '<option value="16">16</option>';
			echo '<option value="17">17</option>';
			echo '<option value="18">18</option>';
			echo '<option value="19">19</option>';
			echo '<option value="20">20</option>';
			echo '<option value="21">21</option>';
			echo '<option value="22">22</option>';
			echo '<option value="23">23</option>';
			echo '<option value="24">24</option>';
			echo '<option value="25">25</option>';
			echo '<option value="26">26</option>';
			echo '<option value="27">27</option>';
			echo '<option value="28">28</option>';
			echo '<option value="29">29</option>';
			echo '<option value="30">30</option>';
			echo '<option value="31">31</option>';
		echo '</select>';
		echo '&nbsp;';
		echo '<select name="mes">';
			echo '<option selected=" "> </option>';
			echo '<option value="1">Enero</option>';
			echo '<option value="2">Febrero</option>';
			echo '<option value="3">Marzo</option>';
			echo '<option value="4">Abril</option>';
			echo '<option value="5">Mayo</option>';
			echo '<option value="6">Junio</option>';
			echo '<option value="7">Julio</option>';
			echo '<option value="8">Agosto</option>';
			echo '<option value="9">Septiembre</option>';
			echo '<option value="10">Octubre</option>';
			echo '<option value="11">Noviembre</option>';
			echo '<option value="12">Diciembre</option>';
		echo '</select>';
		echo '&nbsp;';
		echo '<select name="anio">';
		echo '<option selected=" "> </option>';
			echo '<option value="2009">2009</option>';
			echo '<option value="2010">2010</option>';
			echo '<option value="2011">2011</option>';
			echo '<option value="2012">2012</option>';
			echo '<option value="2013">2013</option>';
			echo '<option value="2014">2014</option>';
			echo '<option value="2015">2015</option>';
			echo '<option value="2016">2016</option>';
			echo '<option value="2017">2017</option>';
			echo '<option value="2018">2018</option>';
	        echo '<option value="2019">2019</option>';
			echo '<option value="2020">2020</option>';
			echo '<option value="2021">2021</option>';
			echo '<option value="2022">2022</option>';
		echo '</select></center>';      
		
		echo '<br><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hasta la fecha&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  ';
		echo '<select name="dia2">';
			echo '<option selected=" "> </option>';
			echo '<option value="1">1</option>';
			echo '<option value="2">2</option>';
			echo '<option value="3">3</option>';
			echo '<option value="4">4</option>';
			echo '<option value="5">5</option>';
			echo '<option value="6">6</option>';
			echo '<option value="7">7</option>';
			echo '<option value="8">8</option>';
			echo '<option value="9">9</option>';
			echo '<option value="10">10</option>';
			echo '<option value="11">11</option>';
			echo '<option value="12">12</option>';
			echo '<option value="13">13</option>';
			echo '<option value="14">14</option>';
			echo '<option value="15">15</option>';
			echo '<option value="16">16</option>';
			echo '<option value="17">17</option>';
			echo '<option value="18">18</option>';
			echo '<option value="19">19</option>';
			echo '<option value="20">20</option>';
			echo '<option value="21">21</option>';
			echo '<option value="22">22</option>';
			echo '<option value="23">23</option>';
			echo '<option value="24">24</option>';
			echo '<option value="25">25</option>';
			echo '<option value="26">26</option>';
			echo '<option value="27">27</option>';
			echo '<option value="28">28</option>';
			echo '<option value="29">29</option>';
			echo '<option value="30">30</option>';
			echo '<option value="31">31</option>';
		echo '</select>';
		echo '&nbsp;';
		echo '<select name="mes2">';
			echo '<option selected=" "> </option>';
			echo '<option value="1">Enero</option>';
			echo '<option value="2">Febrero</option>';
			echo '<option value="3">Marzo</option>';
			echo '<option value="4">Abril</option>';
			echo '<option value="5">Mayo</option>';
			echo '<option value="6">Junio</option>';
			echo '<option value="7">Julio</option>';
			echo '<option value="8">Agosto</option>';
			echo '<option value="9">Septiembre</option>';
			echo '<option value="10">Octubre</option>';
			echo '<option value="11">Noviembre</option>';
			echo '<option value="12">Diciembre</option>';
		echo '</select>';
		echo '&nbsp;';
		echo '<select name="anio2">';
		echo '<option selected=" "> </option>';
			echo '<option value="2009">2009</option>';
			echo '<option value="2010">2010</option>';
			echo '<option value="2011">2011</option>';
			echo '<option value="2012">2012</option>';
			echo '<option value="2013">2013</option>';
			echo '<option value="2014">2014</option>';
			echo '<option value="2015">2015</option>';
			echo '<option value="2016">2016</option>';
			echo '<option value="2017">2017</option>';
			echo '<option value="2018">2018</option>';
	        echo '<option value="2019">2019</option>';
			echo '<option value="2020">2020</option>';
			echo '<option value="2021">2021</option>';
			echo '<option value="2022">2022</option>';
		echo '</select></center>';      
				
		echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';          
	    	echo '<br><center><input type=\'submit\' value="Buscar"></center>';
	     	echo '</form>';
	    	

		
		// Funci�n que comprueba la fecha introducida
		
		if (!checkdate($mes, $dia, $anio) || !checkdate($mes2, $dia2, $anio2)) {	      	
			
			echo '<center>';
				echo '<div class="errorbox">';
				echo 'La fecha introducida no es correcta, por favor introd&uacute;zcala de nuevo';
				echo '</div>';
			echo '<br></center>'; 
			
			$buscar = 'intervalofechas_nook';
			
		}
		
		$fechaseleccionada=$anio.'-'.$mes.'-'.$dia;
		$fechaseleccionada2=$anio2.'-'.$mes2.'-'.$dia2;
		
		if ($fechaseleccionada == '2-2-2' || $fechaseleccionada2 == '2-2-2') {
		
		}
		else {
        $fechaseleccionada = strtotime($fechaseleccionada);
        $fechaseleccionada2 = strtotime($fechaseleccionada2);
		if ($grafico == 'campana' || $grafico == 'error' || $grafico == 'lista' || $mostrar == 'ok' || $buscar == 'intervalofecha') {
	if ($buscar == 'intervalofecha') {
	
			echo "<h3><center>Fechas seleccionadas: desde ".$fechaseleccionada." hasta ".$fechaseleccionada2."</center></h3><br />";
            		
			print_graph_data_table("SELECT * FROM ".$CFG->prefix."gymkana_games WHERE date BETWEEN '$fechaseleccionada' AND '$fechaseleccionada2'  AND gymkana='$gymkana->id' AND user='$USER->id'");
	} 
	
		$query = "SELECT AVG(score) FROM ".$CFG->prefix."gymkana_games WHERE date BETWEEN '$fechaseleccionada' AND '$fechaseleccionada2' AND gymkana='$gymkana->id' AND user='$USER->id'";
		$results = mysql_query($query);
		$num_results = mysql_num_rows($results);
		if ($num_results > 0) {
			$row = mysql_fetch_row($results); 
			$media = $row[0];
			$media=$media;
			//redondeamos la media a 4 decimales
			$media = (round($media * 10000) / 10000); 	
		}
		
	if ($grafico == 'campana') {

	//consulta que halla la media de la columna de puntuaciones total
		
		
		$query2 = "SELECT SUM(score) FROM ".$CFG->prefix."gymkana_games WHERE date BETWEEN '$fechaseleccionada' AND '$fechaseleccionada2' AND gymkana='$gymkana->id' AND user='$USER->id'";
		$results2 = mysql_query($query2);
		$num_results2 = mysql_num_rows($results2);
		if ($num_results2 > 0) {
			$row2 = mysql_fetch_row($results2); 
			$suma = $row2[0];
			$primertermino = $suma;
			$segundotermino = $media*$media;
			$varianza = ($primertermino - $segundotermino)/$num_results2;
					//redondeamos la varianza a 4 decimales
			$varianza = (round($varianza * 10000) / 10000); 
				
			$desviacion = sqrt($varianza);
			//redondeamos la desviacion a 4 decimales
			$desviacion = (round($desviacion * 10000) / 10000);	
		}
	if ($desviacion != 0) {
		$media = $media * 10000; //Se pasan 4 decimales
		$desviacion = $desviacion * 10000; //Se pasan 4 decimales
					
		echo "<br><center><img src='$CFG->wwwroot/mod/gymkana/grafico_media.php?gymkananame=$gymkananame&course->id=$course->id&gymkana->id=$gymkana->id&course->shortname=$course->shortname&cm->id=$cm->id&media=$media&desviacion=$desviacion&action=$action&grafico=campana&usuario=usuario&buscar=fecha&fechaseleccionada=$fechaseleccionada&mes=$mes&dia=$dia&anio=$anio'/></center><br><br><br>"; 

	}
	else {
		echo '<center>';
		echo '<div class="errorbox">';
		echo 'Esta grafica no tiene sentido si la desviacion es 0';
		echo '</div>';
		echo '<br></center>';
		 
	}
			} //Fin grafico campana
			
			//Grafico error
		if ($grafico == 'error') {
			//consulta que halla la media de la columna de puntuaciones total
			$query2 = "SELECT score FROM ".$CFG->prefix."gymkana_games WHERE date BETWEEN '$fechaseleccionada' AND '$fechaseleccionada2' AND gymkana='$gymkana->id' AND user='$USER->id' ORDER BY score";
			$results2 = mysql_query($query2);
			$num_results2 = mysql_num_rows($results2);

			$aux = $num_results2/2;
				$aux = floor($aux);
			$aux2 = $num_results2/(4);
				$aux2 = floor($aux2);
			$aux3 = $num_results2*(3/4);
				$aux3 = floor($aux3);
			$aux4 = $num_results2/4;
				$aux4 = floor($aux4);
			$aux5 = $num_results2*(3/4);
				$aux5 = floor($aux5);
		
			
			for ($i=0; $i<$num_results2; $i++){			
				$row2 = mysql_fetch_array ($results2);		
				if ($i==0){
					$min = $row2['score'];
					}
				if ($i==($num_results2-1)){
					$max = $row2['score'];
					}

				if ($i==($aux-1)){
					$menos = $row2['score'];
					}
				if ($i==($aux)){
					$igual = $row2['score'];
					}
				if ($i==($aux+1)){
					$mas = $row2['score'];
					}
				if ($i==($aux2)){
					$igual2 = $row2['score'];
					}
				if ($i==($aux3)){
					$igual3 = $row2['score'];					
					}
				if ($i==($aux4-1)){
					$menos4 = $row2['score'];
					}
				if ($i==($aux4)){
					$igual4 = $row2['score'];
					}
				if ($i==($aux5-1)){
					$menos5 = $row2['score'];
					}
				if ($i==($aux5)){
					$igual5 = $row2['score'];
					}
				if ($i==($aux5+1)){
					$mas5 = $row2['score'];
					}		
			}
			
			if ($num_results2 > 3) { // Solo muestra la grafica si hay mas de 3 resultados
		        if($num_results2 % 2 == 0) //numero par de notas
		        {
			        $valor = $num_results2/2;
			        $Q2 = ($igual+$menos)/2;
			        if($valor % 2 != 0) //numero impar
			    	{
			       		$Q1=$igual2;
			       		$Q3=$igual3;
			       	}
			       	else //numero par
			       	{
			      		$Q1 = ($igual4+$menos4)/2;
			      		$Q3 = ($igual5+$menos5)/2;	
		       	  	}
			    }
			    else //numero impar de notas
		        {
			    	$valor = $num_results2/2; 
			    	$valor = floor($valor); //obtiene parte entera
			    	$Q2=$igual;
			    	if($valor % 2 != 0)
			    	{
			       		$Q1=$igual2;
			       		$Q3=$igual3;
			       	}
			       	else
			       	{
			      		$Q1 = ($igual4+$menos4)/2;
			      		$Q3 = ($igual5+$mas5)/2;		       	
		       	  	}	
				} 
	
				$Q1 = round($Q1 * 100); //redondeo 2 decimales y multiplico por 100 para pasarlo como entero
				$Q2 = round($Q2 * 100);
				$Q3 = round($Q3 * 100);	

				$min = $min*100; //multiplico por 100 para pasarlo como entero
				$max = $max*100;
				$media = $media*100;

			    echo "<center><br><img src='$CFG->wwwroot/mod/gymkana/grafico_error.php?gymkananame=$gymkananame&course->id=$course->id&gymkana->id=$gymkana->id&course->shortname=$course->shortname&cm->id=$cm->id&Q1=$Q1&Q2=$Q2&Q3=$Q3&max=$max&min=$min&media=$media&action=$action&grafico=$error&usuario=Todos&fechaseleccionada=$fechaseleccionada&mes=$mes&dia=$dia&anio=$anio' /><br><br><br></center>";
			}
	        else
	        {
				echo '<center>';
				echo '<div class="errorbox">';
				echo 'Error, Esta grafica solo tiene sentido con m&aacute;s de 3 resultados';
				echo '</div>';
				echo '<br></center>';  
	    	}

}//fin grafico error


/// Gr�fico lista		
if ($grafico == 'lista') {

			$query3 = "SELECT user, score FROM ".$CFG->prefix."gymkana_games WHERE date BETWEEN '$fechaseleccionada' AND '$fechaseleccionada2' AND gymkana='$gymkana->id' AND user='$USER->id'";
			$results3 = mysql_query($query3);
			$num_results3 = mysql_num_rows($results3);
			
			//print_object($punt);
			//echo $numnotas.'<br>';
			$numnotas= ceil($num_results3/10); //redondea hacia arriba
			$anterior=1;
			echo '<br><b>Mostrar resultados del : </b>';
			for($i=1;$i<$numnotas+1;$i++)
			{
				$io=10*$i;
				if($numnotas!=$i)
				{
					echo '<font color=black><a href="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action=fecha&grafico=lista&mostrar=ok&primero='.$anterior.'&io='.$io.'&ultimo='.$ultimo.'&mes='.$mes.'&dia='.$dia.'&anio='.$anio.'&fechaseleccionada='.$fechaseleccionada.'&buscar=fecha&user='.$username.'"><b>'.$anterior.' al'.' '.$io.', </a><b></font>';
				}
				else
				{
					echo '<font color=black><a href="'.$CFG->wwwroot.'/mod/gymkana/graficos_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action=fecha&grafico=lista&mostrar=ok&ultimo=1&primero='.$anterior.'&io='.$io.'&mes='.$mes.'&dia='.$dia.'&anio='.$anio.'&fechaseleccionada='.$fechaseleccionada.'&buscar=fecha&user='.$username.'"><b>'.$anterior.' al '.$num_results3.'. </a><b></font>';
				}
				$anterior=$io+1;
			}
		
			//$numero=1;
			for($a=2;$a<12;$a++){
				
				$punt=mysql_fetch_array($results3);
				$punt[$a]->puntuacion = $punt['score'];
				$punt[$a]->usuario = $punt['user'];
				
				//introduce 10 valores en datos, si no hay valor mete un cero
					//echo $numero.'<br>';
					if($punt[$a]->usuario == '')
					{
					 	$datos[$a]->usuario = ' ';
					 	$datos[$a]->puntuacion = 0;
				 	}
				 	else
				 	{
						$datos[$a]->usuario= $punt[$a]->usuario;
						$datos[$a]->puntuacion= $punt[$a]->puntuacion;
					}
				}
				echo'<br><br>';
				
			}
			
			if ($mostrar == 'ok'){
			
				$max=10;
				$min=0;
								
				$nombre1=$datos[2]->usuario;
				$nombre2=$datos[3]->usuario;
				$nombre3=$datos[4]->usuario;
				$nombre4=$datos[5]->usuario;
				$nombre5=$datos[6]->usuario;
				$nombre6=$datos[7]->usuario;
				$nombre7=$datos[8]->usuario;
				$nombre8=$datos[9]->usuario;
				$nombre9=$datos[10]->usuario;
				$nombre10=$datos[11]->usuario;
				
						 
				$nota1=$datos[2]->puntuacion*100; //paso la nota como entero (tiene 2 decimales)
				$nota2=$datos[3]->puntuacion*100;
				$nota3=$datos[4]->puntuacion*100;
				$nota4=$datos[5]->puntuacion*100;
				$nota5=$datos[6]->puntuacion*100;
				$nota6=$datos[7]->puntuacion*100;
				$nota7=$datos[8]->puntuacion*100;
				$nota8=$datos[9]->puntuacion*100;
				$nota9=$datos[10]->puntuacion*100;
				$nota10=$datos[11]->puntuacion*100;
						
				$max = $max * 100;
				$min = $min * 100;
				
							
				//echo 'MAX:'.$max.'MIN:'.$min.'<br>';
											
				echo "<center><br><img src='$CFG->wwwroot/mod/gymkana/grafico_lista.php?gymkananame=$gymkananame&course->id=$course->id&gymkana->id=$gymkana->id&course->shortname=$course->shortname&cm->id=$cm->id&action=$action&grafico=lista&mostrar=ok&usuario=$usuario&nivelseleccionado=$nivelseleccionado&nam1=$nombre1&nam2=$nombre2&nam3=$nombre3&nam4=$nombre4&nam5=$nombre5&nam6=$nombre6&nam7=$nombre7&nam8=$nombre8&nam9=$nombre9&nam10=$nombre10&not1=$nota1&not2=$nota2&not3=$nota3&not4=$nota4&not5=$nota5&not6=$nota6&not7=$nota7&not8=$nota8&not9=$nota9&not10=$nota10&max=$max&min=$min&numero=$numero&numnotas=$numnotas&ultimo=$ultimo' /><br><br><br></center>";
			
			} // Fin gr�fico lista
			} //Fin gr�ficos			
		
		}
}



if ($grafico == 'nada'){
}

//Grafico de la media de todos los valores




// Print the main part of the page	
	//	echo "<img src='ejemplo1.php'>";
		
/// Finish the page
	print_footer($course);

?>