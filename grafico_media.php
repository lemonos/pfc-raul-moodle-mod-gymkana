<?php

	require_once("../../config.php");
	require_once("lib.php");
	include_once("lib/phplot/phplot.php");

//Inicializamos las variables


$gymkananame = optional_param('gymkananame', 0, PARAM_CLEAN); 
$course->id  = optional_param('course->id', 0, PARAM_INT);  
$gymkana->id  = optional_param('gymkana->id', 0, PARAM_INT);  
$course->shortname = optional_param('course->shortname', 0, PARAM_CLEAN);
$cm->id = optional_param('cm->id', 0, PARAM_INT);
    $a  = optional_param('a', 0, PARAM_INT);  // gymkana ID

    if ($cm->id) {
        if (! $cm = get_record("course_modules", "id", $cm->id)) {
            error("Course Module ID was incorrect");
        }
		if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
   
    } 


$desviacion = optional_param('desviacion', 0, PARAM_INT);
$media = optional_param('media', 0, PARAM_INT);

		 require_course_login($course);
		  $context = get_context_instance(CONTEXT_MODULE, $cm->id);


$max = 0;
$desviacion = $desviacion / 10000; //Recupero 4 decimales
$media = $media / 10000; //Recupero 4 decimales

$data = array();
	
for ($i=-100;$i<101;$i++) //Se calcula la funcion
	{
	 	$f = (1/((sqrt(2*M_PI))*$desviacion)) * exp((-1/2) * (((($i/10)-$media)/$desviacion)*((($i/10)-$media)/$desviacion))); //formula de la distribucion normal
		$f = round($f * 1000) / 1000; //redondeo 3 decimales
		$data[] = array('',($i/10), $f); //se guarda en array $data
		if ($f > $max)
		{
			$max = $f;
		}
    }

$ejeY = round((((1/3)*$max)+$max) * 100) / 100;  //Proporcionamos el eje Y en torno al valor maximo

$plot = new PHPlot(700, 400);

$plot->SetImageBorderType('plain');

$plot->SetPlotType('lines');
$plot->SetDataType('data-data');
$plot->SetDataValues($data);

# Main plot title:
$plot->SetTitle('Nota Media: Distribucion normal');
$plot->SetXTitle('Nota');
$plot->SetYTitle('Frecuencia');

# Make sure Y axis starts at 0:
$plot->SetPlotAreaWorld(-10, 0, 10, $ejeY);
$plot->SetLineWidth(1);


$plot->SetXDataLabelPos('none');
$plot->SetDrawXGrid(True);

$plot->DrawGraph();


?>