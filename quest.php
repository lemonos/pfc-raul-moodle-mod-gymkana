<?php

	require_once("../../config.php");
	require_once("lib.php");
	
	$gymkananame = optional_param('gymkananame', 0, PARAM_CLEAN); 
    $course->id  = optional_param('course->id', 0, PARAM_INT);  
	$gymkana->id  = optional_param('gymkana->id', 0, PARAM_INT);  
	$course->shortname = optional_param('course->shortname', 0, PARAM_CLEAN);
	$cm->id = optional_param('cm->id', 0, PARAM_INT);
    $a  = optional_param('a', 0, PARAM_INT);  // gymkana ID

    if ($cm->id) {
        if (! $cm = get_record("course_modules", "id", $cm->id)) {
            error("Course Module ID was incorrect");
        }
		if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
   
    } 

		 
  require_course_login($course);
  $context = get_context_instance(CONTEXT_MODULE, $cm->id);

    add_to_log($course->id, "gymkana", "view", "quest.php?id=".$cm->id, $gymkana->id);
	
/// Print the page header

    $strgymkanas = get_string("modulenameplural", "gymkana");
    $strgymkana  = get_string("modulename", "gymkana");

	$navigation1 = "<a href=\"index.php?id=$course->id\">$strgymkanas</a> ->";			  
	$navigation2 = "<a href=\"view.php?a=$gymkana->id\">".stripslashes(format_string($gymkana->name))."</a> -> ";
	print_header_simple($gymkananame." - B&uacute;squeda", "",
                 "$navigation1 "."$navigation2 "."Editar Retos");


    //INTRODUZCO PESTAÑAS
    $currenttab = 'editquests';
    include ('tabs.php'); 
    
    // Print the main part of the page	

		require ('ppal_quest.php');
		
/// Finish the page
	print_footer($course);

?>