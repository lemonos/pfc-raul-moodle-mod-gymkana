<?php // $Id: index.php,v 1.7 2007/09/03 12:23:36 jamiesensei Exp $
/**
 * This page lists all the instances of gymkana in a particular course
 *
 * @author
 * @version $Id: index.php,v 1.7 2007/09/03 12:23:36 jamiesensei Exp $
 * @package gymkana
 **/

    require_once("../../config.php");
    require_once("lib.php");


    $cmid = optional_param('cm', 0, PARAM_INT); // Course Module ID, or
    
    if ($cmid) {
        if (! $cm = get_record("course_modules", "id", $cmid)) {
            error("Course Module ID was incorrect");
        }
        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
   
    } 
    

  //require_login($course->id);
  require_course_login($course);
  $context = get_context_instance(CONTEXT_MODULE, $cm->id);
  

  getHeader(format_string($gymkana->name), array (get_string("modulenameplural", "gymkana")), false);


/// Get all the appropriate data

    if (! $gymkanas = get_all_instances_in_course("gymkana", $course)) {
        notice("There are no gymkanas", "../../course/view.php?id=$course->id");
        die;
    }

/// Print the list of instances (your module will probably extend this)

    $timenow = time();
    $strname  = get_string("name");
    $strweek  = get_string("week");
    $strtopic  = get_string("topic");

    if ($course->format == "weeks") {
        $table->head  = array ($strweek, $strname);
        $table->align = array ("center", "left");
    } else if ($course->format == "topics") {
        $table->head  = array ($strtopic, $strname);
        $table->align = array ("center", "left", "left", "left");
    } else {
        $table->head  = array ($strname);
        $table->align = array ("left", "left", "left");
    }

    foreach ($gymkanas as $gymkana) {
        if (!$gymkana->visible) {
            //Show dimmed if the mod is hidden
            $link = "<a class=\"dimmed\" href=\"view.php?id=$gymkana->coursemodule\">$gymkana->name</a>";
        } else {
            //Show normal if the mod is visible
            $link = "<a href=\"view.php?id=$gymkana->coursemodule\">$gymkana->name</a>";
        }

        if ($course->format == "weeks" or $course->format == "topics") {
            $table->data[] = array ($gymkana->section, $link);
        } else {
            $table->data[] = array ($link);
        }
    }

    echo "<br />";

    print_table($table);

/// Finish the page

    print_footer($course);

?>
