<?php
	require_once("../../config.php");
	require_once("lib.php");
	require_once("lib/phplot/phplot.php");


//Inicializamos las variables
$id = optional_param('id', 0, PARAM_INT); // Course Module ID, or
$a  = optional_param('a', 0, PARAM_INT);  // newmodule ID
$porcentaje = optional_param('porcentaje', 0, PARAM_INT);
$bien = optional_param('bien',PARAM_INT);
$mal = optional_param('mal',PARAM_INT);
$cm->id = optional_param('cm->id', 0, PARAM_INT);
    $a  = optional_param('a', 0, PARAM_INT);  // gymkana ID

    if ($cm->id) {
        if (! $cm = get_record("course_modules", "id", $cm->id)) {
            error("Course Module ID was incorrect");
        }
		if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
   
    } 



$data = array(
  array(get_string("successes", "gymkana"), $bien),
  array(get_string("failures", "gymkana"), $mal),
);


# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.

$plot = new PHPlot(700,400);
$plot->SetImageBorderType('plain');

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);

# Set enough different colors;
$plot->SetDataColors(array('#2EFE9A', '#FA5858', '#F3F781'));

# Main plot title:
$plot->SetTitle(get_string("succespercent", "gymkana") .$porcentaje.'%');

# Build a legend from our data array.
# Each call to SetLegend makes one line as "label: value".

foreach ($data as $row)
  $plot->SetLegend(implode(': ', $row));

$plot->DrawGraph();

?>