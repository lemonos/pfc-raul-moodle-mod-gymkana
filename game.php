<?php
require_once("../../config.php");
require_once("lib.php");

//parte de request (recogida de datos por get o post)

$cmid = optional_param('cmid', 0, PARAM_INT);  
$accion =  optional_param('action', 'answer', PARAM_ALPHA);
$level = optional_param('level', 1, PARAM_INT);
$gameid = optional_param('gameid', 0, PARAM_INT);
$questid = optional_param('questid', 0, PARAM_INT);
    
$answer = optional_param('answer', null, PARAM_RAW);        
    
    //parte de segguridad de moodle para comprobar que estamos en el curso adecuado.

if ($cmid) {
    if (! $cm = get_record("course_modules", "id", $cmid)) {
        error("Course Module ID was incorrect");
    }
    if (! $course = get_record("course", "id", $cm->course)) {
        error("Course is misconfigured");
    }

    if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
        error("Course module is incorrect");
    }

} 
    
require_course_login($course);
$context = get_context_instance(CONTEXT_MODULE, $cm->id);

if ($accion == 'showhelp') {
    print_header();
    print_heading( get_string('help') );
    print_simple_box_start();
       
    $q = get_record("gymkana_quest", "id", $questid);
    echo ( $q->help );
    
    print_simple_box_end();
    print_footer('none');
    die();
}

if ( !empty($questid) ) {
  if (! $quest = get_record("gymkana_quest", "id", $questid)) {
        error("Quest ID was incorrect");
    }     
}

$courseid  = $course->id;
$gymkananame = $gymkana->name;
$gymkanaid = $gymkana->id; 


            
/// Print the page header            
    $navigation = array ();
    $navigation[] = "Nivel {$level}";
                 
    getHeader(format_string($gymkana->name), $navigation, true);             
             
require_once('./game_form.php');    

//primer caso la respuesta
switch ($accion) {
    case 'answer':

        if ($gameid == 0) {
           $params = new object();
           $params->gymkana = $gymkana->id;   
           $params->user =$USER->id;
           $params->initdate = time();  
           $gameid = insert_record("gymkana_games", $params);        //Inserto en la BD gymkana_result los parámetros
        }  
    
        $query = "SELECT *  FROM {$CFG->prefix}gymkana_quest WHERE gymkana='{$gymkana->id}' AND level='{$level}' "; 
        if (!$gymkana->repeatquestions) 
            $query .= "AND id NOT IN (SELECT idquest FROM {$CFG->prefix}gymkana_game_answers WHERE gameid='{$gameid}') ";
        $query .= "ORDER BY RAND()";
        
        $quest = get_record_sql( $query );

        if (empty($quest->id) ) {
            setGameEndData($gameid, $gymkana->levels);
            
            print_simple_box_start ("center"); 
            print_heading( get_string('gameend', 'gymkana')  );
            echo '<a href="game.php?cmid='.$cmid.'">' . get_string('newgame', 'gymkana') .'</a>';
            print_simple_box_end();
            break;
        }

        $data['cmid'] = $cm->id;
        $data['level'] = $level;
        $data['gameid'] = $gameid; 
        $data['questid'] = $quest->id;
        
        
        $answer_form = new answer_form(null, $data); 
        //pintamos la tabla del eneunciado de la pregunta           
        $table1 -> head = array ( get_string('quest', 'gymkana') );
        $table1->data[] = array ($quest->quest);
        $table1 -> align = array ("left");
        print_table ($table1);
                   
        //pintamos la tabla de para responder a la pregunta propuesta.
        $table3-> head = array (get_string('shortquest', 'gymkana'));  
        //$table3->data[] = array ($quest->shortquest);  
        
        
        
        $path = "game.php?cmid={$cm->id}&questid={$quest->id}&action=showhelp";
        $table3->data[] = array ( $quest->shortquest, "<a id='help' href='{$path}'><img src='images/help.jpg' width='48' height='48'></a>" ); 
        $table3-> align = array ("center");
        
        print_table ($table3);
        $answer_form->display() ;
        
        echo ( getPopupInitScript( array('#help') ) ); 
        
       
 
       break;
            
            
   //------------------------------------------------------------------------------------------------------
  
  
  //siguiente pantalla, subir archivo
                 
    case 'uploadfile':
    
        
        $data = array();
        $data['answer'] = $answer;
        $data['cmid'] = $cm->id;
        $data['level'] = $level;
        $data['gameid'] = $gameid; 
        $data['questid'] = $questid;
            
        $upload_file_form = new upload_file_form(null, $data);
    
        if ( $formdata = $upload_file_form->get_data()) {
            //CREO EL DIRECTORIO DE SUBIDA EN MOODLEDATA
            $path2modata = "{$course->id}/{$CFG->moddata}/gymkana/datausers/{$USER->id}/game_{$gameid}/level_{$level}";
            $upload_dir = make_upload_directory($path2modata);

            //Subo el fichero al directorio creado
            if (!$upload_file_form->save_files($upload_dir))
            {
                $a=new object();
                $a->filename  = $ansfile_form->get_new_filename();
                $a->uploaddir = $upload_dir;
                notify( get_string('file_upload_error','gymkana', $a), 'admin');    
            } else {
                $path = $CFG->wwwroot . '/file.php/' . $path2modata . '/' . $upload_file_form->get_new_filename();

               //grabo en la BD
                $q = get_record( 'gymkana_quest', 'id', $questid);
                $answer_pattern  = $q->shortans;
                //Control de Tiempo
                $current_time =   time();
                
                //recojo parametros
                $params = new object();
                $params->user = $USER->id;
                $params->gameid = $gameid;  
                $params->ansupload = $upload_file_form->get_new_filename(); ; 
                $params->levelquest = $level;  
                $params->idquest = $formdata->questid;
                $params->gymkana = $gymkana->id;  
                $params->date = $current_time; 
                $params->ipuser = getremoteaddr(); 
                $params->answer = $answer;
                
                //parametro trim() elimina los espacios en blanco sobrantes y  strtroupe() me convierte a mayusculas, asi evitamos errores
                //Ademas, aprovecho y ya realizamos la comparación de la repuesta correcta con la que inserta el alumno.
                $params->score = (trim( strtoupper($answer) ) === trim(strtoupper($answer_pattern)) ) ? 1 : 0;
                
                // insertamos todos los parametros en la BD con la funcion de moodle insert record. 
                $answerid = insert_record("gymkana_game_answers", $params); 
      
                print_simple_box_start ("center");
                
                $finjuego = ($level == 5);

         
                $g = get_record( 'gymkana_games', 'id', $gameid); 
                
                $intit_time = $g->initdate;
                                                                            
                $tiempo_utilizado =  ($current_time -  $intit_time);

                if ($tiempo_utilizado > $gymkana->timequest) {
                     setGameEndData($gameid, $gymkana->levels);
                     
                     print_heading( get_string ('maxtimeexceeded','gymkana') );
                     $a = new object();
                     $a->timespent = $tiempo_utilizado;
                     $a->timeuqest = $gymkana->timequest;
                     echo ( get_string('timereport', 'gymkana', $a) );
                     echo ( '<a href="game.php?cmid='.$cmid.'">' . get_string('newgame', 'gymkana') . '</a>' );
                     break;
                }   
                

                
                if ($level != $gymkana->levels){
                    
                    //decidimos is es correcta o no la respuesta
                    print_heading( ($params->score == 1) ? get_string('correctanswer', 'gymkana') : get_string('incorrectanswer', 'gymkana') );
 
                    
                    //pintamos la tabla con la descripción de la pregunta
                    if (!empty($q->file) )  {
                        $q->ans = getEmbebedSourceCode ($q->file, $q->id);
                    } 
                          
                    $table-> head = array (get_string('yoursolution', 'gymkana'), get_string('answer', 'gymkana')); 
                    $table->data[] = array (getEmbebedAnswerSourceCode ($params->ansupload, $params->gameid, $params->user, $level), $q->ans);  
                    $table-> align = array ("left");
                    print_table ($table);
                    
                     
                    echo '<p align="left">';
                    echo ($params->score == 1) ? 
                                            get_string('uplevelrecomendation','gymkana') . ' <img src="images/ok.gif" alt="' . get_string('uplevelrecomendation','gymkana') . '"></p>' : 
                                            get_string('samelevelrecomendation','gymkana') . '<img src="images/ko.gif" alt="' . get_string('samelevelrecomendation','gymkana') . '"><br>' . get_string('codeanalysistext','gymkana') . '</p>'; 
                    if ($params->score == 0) {
                           $data['cmid'] = $cm->id;
                           $data['level'] = $level;
                           $data['gameid'] = $gameid; 
                           $data['label'] = get_string('repeatlevel', 'gymkana') ;
                           $reanswer_form = new next_answer_form(null, $data); 
                           $reanswer_form->display();
                    }
                    $data['cmid'] = $cm->id;
                    $data['level'] = $level+1;
                    $data['gameid'] = $gameid;
                    $data['label'] = get_string('nextlevel', 'gymkana') ;  
                    $next_answer_form = new next_answer_form(null, $data); 
                    if ($params->score != 0 || ($params->score == 0 && $gymkana->allowstudentuplevel ) )
                        $next_answer_form->display();
                }  else {
                    $heading_text =   strtoupper ( get_string('gameend', 'gymkana')  ) . ': ';
                    $heading_text .=   ($params->score == 1) ? get_string('correctanswer', 'gymkana') : get_string('incorrectanswer', 'gymkana');
                    print_heading($heading_text);
                    setGameEndData($gameid, $gymkana->levels);
                    echo ( '<a href="game.php?cmid='.$cmid.'">' . get_string('newgame', 'gymkana') . '</a>' );
                }
                    
                print_simple_box_end();
            }  
        }  else {
             
              print_simple_box_start ("center");
              print_heading( get_string('file2upload', 'gymkana') );
              $upload_file_form->display();
              print_simple_box_end();
 
        }                                   

        break;      
            
}
?>