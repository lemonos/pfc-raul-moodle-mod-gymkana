<?php

require_once($CFG->dirroot.'/lib/formslib.php');       
  
  
class answer_form extends moodleform {

    // Define the form
    function definition() {
        global $USER, $CFG, $COURSE;

        $mform =& $this->_form;

        $customdata =& $this->_customdata;
  
        //answer
        $mform->addElement('text', 'answer', get_string('studentanswer', 'gymkana'), 'size="100"');
        $mform->setType('answer', PARAM_RAW);
        $mform->addRule('answer', get_string('required'), 'required', null, 'client');
        
        
        /// Agrego los campos ocultos necesarios 
                                                 
        //TODO:
        //revisar los datos ocultos
        $mform->addElement('hidden', 'level', $customdata['level']);
        $mform->addElement('hidden', 'gameid', $customdata['gameid']);
        $mform->addElement('hidden', 'questid', $customdata['questid']); 
        $mform->setType('level', PARAM_INT);
        $mform->addElement('hidden', 'cmid', $customdata['cmid']);  
        $mform->addElement('hidden', 'action', 'uploadfile');
        $this->add_action_buttons(false, 'Continuar'); 
    
 } 
       
    function definition_after_data() {
        //global $USER, $CFG;

       // $mform =& $this->_form;
      
    }

    function validation($usernew, $files) {
        //global $CFG;
        
    }
}

//-------------------------------------------------------------------------

class upload_file_form extends moodleform {

    // Define the form
    function definition() {
        global $USER, $CFG, $COURSE;
        $mform =& $this->_form;

        $customdata =& $this->_customdata;
        
        //Hacemos uso de la funcion upload manager de moodle para subir un fichero.
        
        $this->set_upload_manager(new upload_manager('uploadfile', true, false, $COURSE, false, 0, true, true, false));
        $mform->addElement('file', 'uploadfile', 'Subir fichero desarrollado:', 'size="40"');
        $mform->addRule('uploadfile', null, 'required');
                                            
                                    
        
        /// Agrego los campos ocultos necesarios 
 
                                                    
        //TODO:
        //revisar los datos ocultos
        $mform->addElement('hidden', 'answer', $customdata['answer'] );
        $mform->addElement('hidden', 'level', $customdata['level']);
        $mform->addElement('hidden', 'cmid', $customdata['cmid']);  
        $mform->addElement('hidden', 'action', 'uploadfile');
        $mform->addElement('hidden', 'gameid', $customdata['gameid']);
        $mform->addElement('hidden', 'questid', $customdata['questid']); 
        $mform->addElement('hidden', 'action', 'uploadfile');
        $this->add_action_buttons(false, 'Continuar'); 
 
    }
    
        
    function definition_after_data() {
        //global $USER, $CFG;

       // $mform =& $this->_form;
      
    }

    function validation($usernew, $files) {
        //global $CFG;
        
    }
}

//-------------------------------------------------------------------------


 
//-------------------------------------------------------------------------

class next_answer_form extends moodleform {

    // Define the form
    function definition() {
        global $USER, $CFG, $COURSE;
        $mform =& $this->_form;

        $customdata =& $this->_customdata;          
        
        
        /// Agrego los campos ocultos necesarios
        $mform->addElement('hidden', 'level', $customdata['level']);
        $mform->addElement('hidden', 'gameid', $customdata['gameid']);
        $mform->setType('level', PARAM_INT);
        $mform->addElement('hidden', 'cmid', $customdata['cmid']);  
        $mform->addElement('hidden', 'action', 'answer');
        $this->add_action_buttons(false, $customdata['label']); 
 
    }

    function definition_after_data() {
        //global $USER, $CFG;

       // $mform =& $this->_form;
      
    }

    function validation($usernew, $files) {
        //global $CFG;
        
    }
}

//-------------------------------------------------------------------------

class game_form extends moodleform {
    // Define the form
    function definition() {
        global $USER, $CFG, $COURSE;
        $mform =& $this->_form;

        $customdata =& $this->_customdata;          
        
        
        /// Agrego los campos ocultos necesarios
        $mform->addElement('hidden', 'cmid', $customdata['cm_id']);  
        $this->add_action_buttons(false, get_string ('begin_game', 'gymkana')); 
 
    }

    function definition_after_data() {
        //global $USER, $CFG;

       // $mform =& $this->_form;
      
    }

    function validation($usernew, $files) {
        //global $CFG;
        
    }
}
   
?>
