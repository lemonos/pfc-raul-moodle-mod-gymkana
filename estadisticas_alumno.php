<?php  

    require_once("../../config.php");
    require_once("lib.php");

//Inicializamos las variables
  	$gymkananame = optional_param('gymkananame', 0, PARAM_CLEAN); 
    $course->id  = optional_param('course->id', 0, PARAM_INT);  
	$gymkana->id  = optional_param('gymkana->id', 0, PARAM_INT);  
	$course->shortname = optional_param('course->shortname', 0, PARAM_CLEAN);
	$cm->id = optional_param('cm->id', 0, PARAM_INT);  
  
  	$action = optional_param('action', 'opcionesbusqueda', PARAM_ALPHA);
	$buscar = optional_param('buscar', 'nada', PARAM_ALPHA);
  	$usuario = optional_param('usuario', 'opcionesbusqueda', PARAM_ALPHA);
	$level = optional_param('level', 0, PARAM_INT);
	$nivel = optional_param('nivel', 0, PARAM_INT);
	$notapregunta = optional_param('notapregunta', '0', PARAM_ALPHA);
	$bien = optional_param('bien',PARAM_INT);
	$mal = optional_param('mal',PARAM_INT);
  
  
      $a  = optional_param('a', 0, PARAM_INT);  // gymkana ID*/

    if ($cm->id) {
        if (! $cm = get_record("course_modules", "id", $cm->id)) {
            error("Course Module ID was incorrect");
        }
		if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
   
    } 
	
  require_course_login($course);
  $context = get_context_instance(CONTEXT_MODULE, $cm->id);

    add_to_log($course->id, "gymkana", "estadisticas", "estadisticas_alumno.php?id=".$cm->id, $gymkana->id);
	
/// Print the page header

    $strgymkanas = get_string("modulenameplural", "gymkana");
    $strgymkana  = get_string("modulename", "gymkana");

	$navigation1 = "<a href=\"index.php?id=$course->id\">$strgymkanas</a> ->";			  
	$navigation2 = "<a href=\"view.php?a=$gymkana->id\">".stripslashes(format_string($gymkana->name))."</a> -> ";
	print_header_simple($gymkananame." - " .get_string("mystatistics", "gymkana"), "",
                 "$navigation1 "."$navigation2 ".get_string("mystatistics", "gymkana"));
				   

    //INTRODUZCO PESTA�AS
    $currenttab = 'userstatistics';
    include ('tabs.php'); 
                   //Funcion para seleccionar el metodo de busqueda   
?>
<script language="Javascript" type="text/javascript">
	
		function jumpPage(newLoc) 
		{
			newPage = newLoc.options[newLoc.selectedIndex].value
			
			if (newPage != "") 
			{
				window.location = newPage
			}
		}
	</script>            
    
<?php    

  
echo '<center>';
echo '<caption><h2><u>' . get_string("mystatistics", "gymkana") . '</u></h2></caption>';

echo '<br>';	
			echo '<select name="newLocation" onchange="jumpPage(this)">';
			echo '<option>' . get_string("selecttypestatistics", "gymkana") . '</option>';
			echo '<option value="'.$CFG->wwwroot.'/mod/gymkana/estadisticas_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action=todos">' . get_string("allresults", "gymkana") . '</option>';
			echo '<option value="'.$CFG->wwwroot.'/mod/gymkana/estadisticas_alumno.php?gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action=niveles">' . get_string("perlevels", "gymkana") . '</option>';
		echo '</select>';
	echo '</center><br>';

//Fin del menu de seleccion

if ($action == 'todos'){
            $query = "SELECT GA.levelquest AS 'level', GA.idquest AS 'idquest', COUNT(*) AS 'total', sum(case when GA.score='1' then 1 else 0 end) AS 'successes', sum(case when GA.score='0' then 1 else 0 end) AS 'failures'  FROM {$CFG->prefix}gymkana_game_answers as GA INNER JOIN {$CFG->prefix}gymkana_games as GG ON GA.gameid=GG.id INNER JOIN {$CFG->prefix}gymkana as G ON GG.gymkana=G.id WHERE GA.user='{$USER->id}' AND G.name='{$gymkana->name}' GROUP BY GA.idquest ORDER BY GA.levelquest";
            //$results = get_records_sql($query);
            
            $rs = mysql_query($query);
            $results = mysql_num_rows($rs);    
           
           
    
            if (count($results) > 0) {
                $table->head = array (
                                        
                                        get_string("level", "gymkana"),
                                        get_string("answerid", "gymkana"),
                                        get_string("answeroccurrences", "gymkana"),
                                        get_string("successes", "gymkana"),
                                        get_string("failures", "gymkana"),
                                        get_string("wrongpercent", "gymkana")
                                        
                                        );
                while ($result = mysql_fetch_array($rs)) {
                    $table->data[] = array (
                                            $result['level'],
                                            $result['idquest'],
                                            $result['total'],
                                            $result['successes'],
                                            $result['failures'],
                                            round( ($result['failures']/$result['total'])*100 )
                                            );
                   
                }
                
                $table->align = array ("center", "center", "center", "center", "center", "center");

                print_table($table);
                $query2 = "SELECT sum(case when GA.score='1' then GA.score else 0 end) AS 'successes', sum(case when GA.score='0' then 1 else 0 end) AS 'failures' FROM {$CFG->prefix}gymkana_game_answers as GA INNER JOIN {$CFG->prefix}gymkana_games as GG ON GA.gameid=GG.id INNER JOIN {$CFG->prefix}gymkana as G ON GG.gymkana=G.id WHERE GA.user='{$USER->id}' AND G.name='{$gymkana->name}'";
            
        $queryobj =  get_record_sql($query2);  
        $aciertos = $queryobj->successes;
        $fallos = $queryobj->failures;
        $porcentaje = ($aciertos/($aciertos+$fallos))*100;

        echo "<center><br><img src='$CFG->wwwroot/mod/gymkana/estadisticas_usuarios.php?gymkananame=$gymkananame&course->id=$course->id&gymkana->id=$gymkana->id&course->shortname=$course->shortname&cm->id=$cm->id&bien=$aciertos&mal=$fallos&action=usuarios&buscar=usuario&usuario=$usuario&porcentaje=$porcentaje' /><br><br><br></center>";
            } else {
                print_simple_box_start ("center");
                echo get_string("not_found_record", "gymkana");
                print_simple_box_end();
            }
}	//fin busqueda por usuarios


if ($action == 'niveles' || $buscar == 'niveles'){
if ($action == 'niveles'){

$query = "SELECT DISTINCT levelquest FROM ".$CFG->prefix."gymkana_game_answers ORDER BY levelquest";
        $results = mysql_query($query);
        $num_results = mysql_num_rows($results);
        echo '<br>';
    //print_simple_box_start ("center");
    echo "<h3><center>" .get_string("levels", "gymkana") . "</center></h3><center>";        
    if ($num_results > 0) {
            for ($i=0; $i<$num_results; $i++) {
                $row = mysql_fetch_array($results);
                $nivel = stripslashes($row['levelquest']);
                echo '<a href="estadisticas_alumno.php?level='.$nivel.'&gymkananame='.$gymkananame.'&course->id='.$course->id.'&gymkana->id='.$gymkana->id.'&course->shortname='.$course->shortname.'&cm->id='.$cm->id.'&action=niveles&buscar=niveles">Nivel '.$nivel.'</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                
            }
    echo '</center>';        
    } else {
        print_simple_box_start ("center");
        echo get_string("not_found_levels", "gymkana");
        print_simple_box_end();
    }
    

}
    // MUESTRA LOS RESULTADOS DEL USUARIO SELECCIONADO EN EL LISTADO ANTERIOR
    if ($buscar == 'niveles') {
            echo "<br><h4><center>". get_string("levelselected", "gymkana") .$level."</center></h4>";
        
        $query = "SELECT GA.levelquest AS 'level', GA.idquest AS 'idquest', COUNT(*) AS 'total', sum(case when GA.score='1' then 1 else 0 end) AS 'successes', sum(case when GA.score='0' then 1 else 0 end) AS 'failures'  FROM {$CFG->prefix}gymkana_game_answers as GA INNER JOIN {$CFG->prefix}gymkana_games as GG ON GA.gameid=GG.id INNER JOIN {$CFG->prefix}gymkana as G ON GG.gymkana=G.id WHERE GA.levelquest='{$level}' AND GA.user='{$USER->id}' AND G.name='{$gymkana->name}' GROUP BY GA.idquest ORDER BY GA.levelquest";
            //$results = get_records_sql($query);
            
            $rs = mysql_query($query);
            $results = mysql_num_rows($rs); 
           
    
            if (count($results) > 0) {
                $table->head = array (
                                        
                                        get_string("level", "gymkana"),
                                        get_string("answerid", "gymkana"),
                                        get_string("answeroccurrences", "gymkana"),
                                        get_string("successes", "gymkana"),
                                        get_string("failures", "gymkana"),
                                        get_string("wrongpercent", "gymkana")
                                        
                                        );
                while ($result = mysql_fetch_array($rs)) {
                    $table->data[] = array (
                                            $result['level'],
                                            $result['idquest'],
                                            $result['total'],
                                            $result['successes'],
                                            $result['failures'],
                                            round( ($result['failures']/$result['total'])*100 )
                                            );
                   
                }
                
                $table->align = array ("center", "center", "center", "center", "center", "center");

                print_table($table);
                
                $query2 = "SELECT sum(case when GA.score='1' then GA.score else 0 end) AS 'successes', sum(case when GA.score='0' then 1 else 0 end) AS 'failures' FROM {$CFG->prefix}gymkana_game_answers as GA INNER JOIN {$CFG->prefix}gymkana_games as GG ON GA.gameid=GG.id INNER JOIN {$CFG->prefix}gymkana as G ON GG.gymkana=G.id WHERE GA.levelquest='{$level}' AND GA.user='{$USER->id}' AND G.name='{$gymkana->name}'";
            
                $queryobj =  get_record_sql($query2);  
                $aciertos = $queryobj->successes;
                $fallos = $queryobj->failures;
                $porcentaje = ($aciertos/($aciertos+$fallos))*100;
                        
                
                echo "<center><br><img src='$CFG->wwwroot/mod/gymkana/estadisticas_niveles.php?gymkananame=$gymkananame&course->id=$course->id&gymkana->id=$gymkana->id&course->shortname=$course->shortname&cm->id=$cm->id&bien=$aciertos&mal=$fallos&action=usuarios&buscar=usuario&level=$level&porcentaje=$porcentaje' /><br><br><br></center>";
            } else {
                print_simple_box_start ("center");
                echo get_string("not_found_record", "gymkana");
                print_simple_box_end();
            }
    
}    

}    //fin busqueda por niveles

// Final de p�gina
    print_footer($course);
?>