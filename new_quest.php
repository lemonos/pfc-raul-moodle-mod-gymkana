<?php
	
	require_once("../../config.php");
	require_once("lib.php");

 
    $cm_id = optional_param('cmid', 0, PARAM_INT);  
    $accion =  optional_param('action', 'default', PARAM_ALPHA);
    $level = optional_param('level', null, PARAM_INT);
    
    $qid = optional_param('qid', 0, PARAM_INT);
    
    
    if (!empty($cm_id) ) {
        if (! $cm = get_record("course_modules", "id", $cm_id)) {
            error("Course Module ID was incorrect");
        }
		if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("gymkana id is incorrect");
        }
   
    }
  
  require_course_login($course);
  $context = get_context_instance(CONTEXT_MODULE, $cm->id);

    add_to_log($course->id, "gymkana", "view", "ansfile.php?id=".$cm->id, $gymkana->id);
	
/// Print the page header            
    $navigation = array ();
    $navigation[] = "<a href=\"view.php?a=$gymkana->id\">".$gymkana->name."</a>";
    $navigation[] = get_string ('addquest', 'gymkana');
                 
    getHeader(format_string($gymkana->name), $navigation, false);
                 
    //INTRODUZCO PESTAÑAS
    $currenttab = 'addquest';
    include ('tabs.php'); 


 require_once('./new_quest_form.php'); 

 
switch ($accion) {
     case 'ansfile':
    
        $data = array();
        $data['gymkana'] = $gymkana->id;
        $data['courseid'] = $course->id;
        $data['moduleid'] = $cm->id;
        $data['level'] = $level;
        
        $ansfile_form = new ansfile_form(null, $data);
        
        if ( $formdata = $ansfile_form->get_data()) {

            
            
            $newquest = new object();
            $newquest->gymkana = $gymkana->id;
            $newquest->level = $level;

            $questid = addQuest($newquest);
            
            
            //CREO EL DIRECTORIO DE SUBIDA EN MOODLEDATA
            $path2modata = "{$course->id}/{$CFG->moddata}/gymkana/questdata/quest_{$questid}";
            $upload_dir = make_upload_directory($path2modata);
            
            //Subo el fichero al directorio creado
            if (!$ansfile_form->save_files($upload_dir)) {
                $a=new object();
                $a->filename  = $ansfile_form->get_new_filename();
                $a->uploaddir = $upload_dir;
                notify(get_string('file_upload_error','gymkana', $a),'admin');   
                deleteQuest($questid);
            } else {
                $path = $CFG->wwwroot . '/file.php/' . $path2modata . '/' . $ansfile_form->get_new_filename();
                
                $data = array();
                $data['gymkana'] = $gymkana->id;
                $data['courseid'] = $course->id;
                $data['moduleid'] = $cm->id;
                $data['level'] = $level;
                $data['file']  =  $ansfile_form->get_new_filename();
                $data['questid'] = $questid;
       
                $ans_form = new ans_form(null, $data);
                print_simple_box_start ("center");
                print_heading(get_string('new_quest', 'gymkana') );
                $ans_form->display();
                print_simple_box_end();
                                      
            }
        } else {
              print_simple_box_start ("center");
              print_heading(get_string('new_quest', 'gymkana') );
              $ansfile_form->display();
              print_simple_box_end();
            
        }
        
         break;
         
     //FORMULARIO PARA PREGUNTA CORTA/PREGUNTA LARGO   
     case 'ans':
    
        $data = array();
        $data['gymkana'] = $gymkana->id;
        $data['courseid'] = $course->id;
        $data['moduleid'] = $cm->id;
        $data['level'] = $level;
        $data['questid'] = $qid;
    
        
        
        $ans_form = new ans_form(null, $data);
    
        if ( $formdata = $ans_form->get_data()) {
           $params = new object();
           $params->quest = $formdata->quest;   // Create a default section.
           $params->ans = $formdata->ans;
           $params->help = $formdata->help;
           $params->shortquest = $formdata->shortquest;
           $params->shortans = $formdata->shortans;
           $params->gymkana = $gymkana->id;
           $params->level = $level; 
           
           if (empty ($formdata->qid) ) {
               $params->id = addQuest ($params); 
               
           } else {
               $params->id = $formdata->qid;
               $params->file = $formdata->file; 
               updateQuest($params);
           }
   
           

            echo '<br>' . get_string('enter_quest_result', 'gymkana') . '<br><br>';

            $table->head = array (get_string('module_name', 'gymkana'), get_string('level', 'gymkana'), get_string('quest', 'gymkana'), get_string('answer', 'gymkana'), get_string('help', 'gymkana'), get_string('shortquest', 'gymkana'),get_string('shortanswer', 'gymkana'));
            if ( !empty($formdata->file) ) {
                $formdata->ans = getEmbebedSourceCode( $formdata>file, $formdata->id );
            }
            
            
            $table->data[] = array (stripslashes($gymkana->name), $level, stripslashes($formdata->quest), stripslashes($formdata->ans), stripslashes($formdata->help), stripslashes($formdata->shortquest), stripslashes($formdata->shortans));
            $table->align = array ("center", "center", "center", "center", "center", "center", "center");

            print_table($table);
            
            echo '<br><br><ul><li><a href="new_quest.php?cmid='.$cm->id.'">' . get_string('other_quest','gymkana') . '</a></li></ul>';
    
    
        }  else {
              print_simple_box_start ("center");
              print_heading(get_string('new_quest', 'gymkana') );
              $ans_form->display();
              print_simple_box_end();
        }

        break;

     //FORMULARIO POR DEFECTO QUE ME PIDE SELECCIóN DE NIVEL Y TIPO DE PREGUNTA    
     default:
   
        $data = array();
        $data['gymkana'] = $gymkana->id;
        $data['courseid'] = $course->id;
        $data['moduleid'] = $cm->id;
        $data['maxlevels'] = $gymkana->levels;
   
        $new_quest_form = new new_quest_form(null, $data);
        
          print_simple_box_start ("center");
          print_heading(get_string('new_quest', 'gymkana') );
          $new_quest_form->display();
          print_simple_box_end();
    
        break;
}

	/// Finish the page
	print_footer($course);

?>
    