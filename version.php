<?php // $Id: version.php,v 1.5 2007/04/01 22:57:16 stronk7 Exp $

/**
 * Code fragment to define the version of gymkana
 * This fragment is called by moodle_needs_upgrading() and /admin/index.php
 *
 * @author Oscar Ruesga
 * @version $Id: version.php,v 1.5 2007/04/01 22:57:16 stronk7 Exp $
 * @package gymkana
 **/

$module->version  = 2012091402;  // The current module version (Date: YYYYMMDDXX)
$module->cron     = 0;           // Period for cron to check this module (secs)

?>
