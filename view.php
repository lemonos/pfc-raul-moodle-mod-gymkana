<?php  // $Id: view.php,v 1.9 2011/05/03 12:23:36 
/**
 * This page prints a particular instance of gymkana
 *
 * @author Raul Criado Manchon
 * @version $Id: view.php,v 1.1.9 2011/05/03 12:23:36 
 * @package gymkana
 **/


    require_once("../../config.php");
    require_once("lib.php");
	
	@ $user = $HTTP_POST_VARS['user'];
	$user = addslashes($user);
	
	$user = $USER -> username;
	
	
    $id = optional_param('id', 0, PARAM_INT); // Course Module ID, or
    $a  = optional_param('a', 0, PARAM_INT);  // gymkana ID

    if ($id) {
        if (! $cm = get_record("course_modules", "id", $id)) {
            error("Course Module ID was incorrect");
        }

        if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("Course module is incorrect");
        }

    } else {
        if (! $gymkana = get_record("gymkana", "id", $a)) {
            error("Course module is incorrect");
        }
        if (! $course = get_record("course", "id", $gymkana->course)) {
            error("Course is misconfigured");
        }
        if (! $cm = get_coursemodule_from_instance("gymkana", $gymkana->id, $course->id)) {
            error("Course Module ID was incorrect");
        }
    }

    require_login($course->id);

    add_to_log($course->id, "gymkana", "view", "view.php?id=$cm->id", $gymkana->id);
	
   
    getHeader(format_string($gymkana->name), array (format_string($gymkana->name)), false);


	$context = get_context_instance(CONTEXT_MODULE, $cm->id);
	
    
    //INTRODUZCO PESTAÑAS
    $currenttab = 'view';
    include ('tabs.php'); 
    
    print_simple_box_start ("center");
    echo $gymkana->intro . '<br>';
    //helpbutton("menu_administrador", "Menu administrador", "gymkana");
    
    require_once ('./game_form.php');
    $data = array();
    $data['cm_id'] = $cm->id;
    $game_form = new game_form('game.php', $data);
    $game_form->display();
    
    print_simple_box_end(); 
    
    echo "<center>";
    echo 'Ayuda alumno';
    helpbutton("menu_alumno", "Menu alumno", "gymkana");

    if (has_capability('mod/gymkana:adminmod', $context)) { 
        echo " | ";
        echo "Ayuda Profesor";
        helpbutton("menu_administrador", "Menu administrador", "gymkana");
    }
    
    echo "</center>"; 

    print_footer($course);
	
		
?>
