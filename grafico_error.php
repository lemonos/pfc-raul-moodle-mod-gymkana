<?php
	
	require_once("../../config.php");
	require_once("lib.php");
	include_once("lib/phplot/phplot.php");

//Inicializamos las variables


$gymkananame = optional_param('gymkananame', 0, PARAM_CLEAN); 
$course->id  = optional_param('course->id', 0, PARAM_INT);  
$gymkana->id  = optional_param('gymkana->id', 0, PARAM_INT);  
$course->shortname = optional_param('course->shortname', 0, PARAM_CLEAN);
$cm->id = optional_param('cm->id', 0, PARAM_INT);  
$id = optional_param('id', 0, PARAM_INT); // Course Module ID, or
$media = optional_param('media', 0, PARAM_INT);
$Q1 = optional_param('Q1',0, PARAM_INT);
$Q2 = optional_param('Q2',0, PARAM_INT);
$Q3 = optional_param('Q3',0, PARAM_INT);
$max = optional_param('max',0, PARAM_INT);
$min = optional_param('min',0, PARAM_INT);
$usuario = optional_param('usuario', PARAM_ALPHA);

    $a  = optional_param('a', 0, PARAM_INT);  // gymkana ID

    if ($cm->id) {
        if (! $cm = get_record("course_modules", "id", $cm->id)) {
            error("Course Module ID was incorrect");
        }
		if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
   
    } 
	
	 require_course_login($course);
	  $context = get_context_instance(CONTEXT_MODULE, $cm->id);

$Q1 = $Q1/100; //Obtenemos el valor con decimales
$Q2 = $Q2/100;
$Q3 = $Q3/100;
$max = $max/100;
$min = $min/100;
$media = $media/100;


if(round($min-1)<-1) //para ajustar el eje Y parte inferior
{
	$min1=0; 
}
else
{
	$min1=$min;
}
if(round($max+1)>10) //para ajustar el eje Y parte superior
{
	$max1=9; 
}
else
{
	$max1=$max;
}

$plot = new PHPlot(700, 400);
$plot->SetImageBorderType('plain');

# Main plot title:
$plot->SetTitle('Diagrama de cajas y bigotes');

# Disable auto-output:
$plot->SetPrintImage(0);
	
	
/////////////////////////////////////////////////////////////////////////////////////// 1
// Introduce la parte del minimo y del max (azul oscuro)
$data = array(
  array('', '0'), array($usuario,'0.7',$Q2,$max-$Q2, $Q2-$min), array(' ', '2')
  );

# Set up area for second plot:
$plot->SetPlotAreaPixels(80, 40, 640, 350);

$plot->SetPlotType('points');
$plot->SetDataType('data-data-error');
$plot->SetDataValues($data);

# Set data range and tick increments to get nice even numbers:
$plot->SetPlotAreaWorld(NULL, (round($min1)-1), NULL, (round($max1)+1));
$plot->SetXTickIncrement(10);
$plot->SetYTickIncrement(1);

$plot->SetYTitle('Nota');

# Draw both grids:
$plot->SetDrawXGrid(True);
$plot->SetDrawYGrid(True);  # Is default

# Set some options for error bars:
$plot->SetErrorBarShape('tee');  # Is default
$plot->SetErrorBarSize(10);
$plot->SetErrorBarLineWidth(2);

# Use a darker color for the plot:
$plot->SetErrorBarColors('navy');
$plot->SetDataColors('black');

# Make the points bigger so we can see them:
$plot->SetPointSizes(0);

$plot->DrawGraph();

///////////////////////////////////////////////////////////////////////////////////////// 2
// Introduce la parte "ancha" correpondientes al Q1,Q2,Q3 (azul claro)
$data = array(
  array('', '0'), array($name,'0.7' , $Q2, $Q3-$Q2, $Q2-$Q1), array(' ', '2')
  );

# Set up area for first plot:
$plot->SetPlotAreaPixels(80, 40, 640, 350);

$plot->SetPlotType('points');
$plot->SetDataType('data-data-error');
$plot->SetDataValues($data);

# Set data range and tick increments to get nice even numbers:
$plot->SetPlotAreaWorld(NULL, (round($min1)-1), NULL, (round($max1)+1));
$plot->SetXTickIncrement(10);
$plot->SetYTickIncrement(1);

# Draw both grids:
$plot->SetDrawXGrid(True);
$plot->SetDrawYGrid(True);  # Is default

# Set some options for error bars:
$plot->SetErrorBarShape('tee');  # Is default
$plot->SetErrorBarSize(0);
$plot->SetErrorBarLineWidth(40);

# Use a darker color for the plot:
$plot->SetDataColors('#58ACFA');
$plot->SetErrorBarColors('#58ACFA');

# Make the points bigger so we can see them:
$plot->SetPointSizes(0);

$plot->DrawGraph();

/////////////////////////////////////////////////////////////////////////////////////// 3
//Introduce la linea que divide los cuartiles o Mediana (verde)
$data = array(
  array('', '0'), array('','0.7',$Q2), array(' ', '2')
  );

$plot->SetPlotAreaPixels(80, 40, 640, 350);

$plot->SetPlotType('points');
$plot->SetDataType('data-data-error');
$plot->SetDataValues($data);

# Set data range and tick increments to get nice even numbers:
$plot->SetPlotAreaWorld(NULL, (round($min1)-1), NULL, (round($max1)+1));
$plot->SetXTickIncrement(10);
$plot->SetYTickIncrement(1);

# Draw both grids:
$plot->SetDrawXGrid(True);
$plot->SetDrawYGrid(True);  # Is default

# Set some options for error bars:
$plot->SetErrorBarShape('tee');  # Is default
$plot->SetErrorBarSize(19);
$plot->SetErrorBarLineWidth(2);

# Use a darker color for the plot:
$plot->SetErrorBarColors('green');
$plot->SetDataColors('green');

# Make the points bigger so we can see them:
$plot->SetPointSizes(0);

$plot->DrawGraph();

/////////////////////////////////////////////////////////////////////////////////////// 4
//Introduce el punto que representa la media arigmetica (rojo)
$data = array(
  array('', '0'), array('','0.7',$media), array(' ', '2')
  );

$plot->SetPlotAreaPixels(80, 40, 640, 350);

$plot->SetPlotType('points');
$plot->SetDataType('data-data-error');
$plot->SetDataValues($data);

# Set data range and tick increments to get nice even numbers:
$plot->SetPlotAreaWorld(NULL, (round($min1)-1), NULL, (round($max1)+1));
$plot->SetXTickIncrement(10);
$plot->SetYTickIncrement(1);

# Draw both grids:
$plot->SetDrawXGrid(True);
$plot->SetDrawYGrid(True);  # Is default

# Set some options for error bars:
$plot->SetErrorBarShape('tee');  # Is default
$plot->SetErrorBarSize(0);
$plot->SetErrorBarLineWidth(0);

# Use a darker color for the plot:
$plot->SetErrorBarColors('red');
$plot->SetDataColors('red');

# Make the points bigger so we can see them:
$plot->SetPointSizes(5);

//Leyenda
$colors = array('red','green','#58ACFA','navy');
$legend = array('Nota media: '.$media, 'Mediana (Q2): '.$Q2, 'Q1 & Q3: '.$Q1.' & '.$Q3, 'Min & Max: '.$min.' & '.$max);
$plot->SetLegendStyle('left', 'left');
$plot->SetDataColors($colors);
$plot->SetLegend($legend);

$plot->DrawGraph();

# Output the image now:
$plot->PrintImage();

?>