<?php //$Id: new_quest_form.php,v 1.0 2012/04/10 20:46:32 rycis Exp $

require_once($CFG->dirroot.'/lib/formslib.php');


class edit_quest_form extends moodleform {

    // Define the form
    function definition() {
        global $USER, $CFG, $COURSE;

        $mform =& $this->_form;

        $customdata =& $this->_customdata;
  

        //Agrego el campo para seleccionar el Nivel
        $options = array ();
  
        for ($i=1;$i<=$customdata['maxlevels'];$i++){
            $options[$i] = $i;
        }

        
        $mform->addElement('select', 'level', get_string('level', 'gymkana'), $options);
        $mform->addRule('level', get_string('required'), 'required', null, 'client');
        
        //quest
        $mform->addElement('htmleditor', 'quest', get_string('quest', 'gymkana'));
        $mform->setType('quest', PARAM_RAW);
        $mform->addRule('quest', get_string('required'), 'required', null, 'client');
        
        //ans
        if (!empty($customdata['file']) ) {
            
            $mform->addElement('checkbox', 'deletefile', get_string('delete'));
            $mform->setDefault('deletefile',false);
            
            
            $mform->addElement('static', 'current_file', get_string('current_file', 'gymkana'), getEmbebedSourceCode( $customdata['file'], $customdata['questid'] ));
            
            

        } else {
            $mform->addElement('htmleditor', 'ans', get_string('answer', 'gymkana'));
            $mform->setType('ans', PARAM_RAW);
            $mform->addRule('ans', get_string('required'), 'required', null, 'client');
        }
        
        $this->set_upload_manager(new upload_manager('ansfile', true, false, $COURSE, false, 0, true, true, false));
        $mform->addElement('file', 'ansfile', get_string('file2upload', 'gymkana'), 'size="40"');
        
        
         //help
        $mform->addElement('htmleditor', 'help', get_string('help', 'gymkana'));
        $mform->setType('help', PARAM_RAW);
        $mform->addRule('help', get_string('required'), 'required', null, 'client');
        
        //shortquest
        $mform->addElement('text', 'shortquest', get_string('shortquest', 'gymkana'), 'size="100"');
        $mform->setType('shortquest', PARAM_RAW);
        $mform->addRule('shortquest', get_string('required'), 'required', null, 'client');
        
        //shortquest
        $mform->addElement('text', 'shortans',  get_string('shortanswer', 'gymkana'), 'size="100"');
        $mform->setType('shortans', PARAM_RAW);
        $mform->addRule('shortans', get_string('required'), 'required', null, 'client');
       
   
        
        //Campos ocultos
        $mform->addElement('hidden', 'cmid', $customdata['moduleid']);
        $mform->addElement('hidden', 'qid', $customdata['questid']);
        $mform->addElement('hidden', 'action', 'edit');
        

        $mform->addElement('hidden', 'id', '');
        
        $this->add_action_buttons(false, get_string('update'));
    }

    function definition_after_data() {
        //global $USER, $CFG;

       // $mform =& $this->_form;
      
    }

    function validation($usernew, $files) {
        //global $CFG;
        
    }
}
?>
