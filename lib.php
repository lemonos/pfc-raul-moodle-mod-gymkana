<?php  // $Id: lib.php,v 1.7 2007/09/18 18:37:08 skodak Exp $
/**
 * Library of functions and constants for module gymkana
 * This file should have two well differenced parts:
 *   - All the core Moodle functions, needed to allow
 *     the module to work integrated in Moodle.
 *   - All the gymkana specific functions, needed
 *     to implement all the module logic. Please, note
 *     that, if the module become complex and this lib
 *     grows a lot, it's HIGHLY recommended to move all
 *     these module specific functions to a new php file,
 *     called "locallib.php" (see forum, quiz...). This will
 *     help to save some memory when Moodle is performing
 *     actions across all modules.
 */


$gymkana_CONSTANT = 17;     /// for example

/**
 * Given an object containing all the necessary data, 
 * (defined by the form in mod.html) this function 
 * will create a new instance and return the id number 
 * of the new instance.
 *
 * @param object $instance An object from the form in mod.html
 * @return int The id of the newly inserted gymkana record
 **/
function gymkana_add_instance($gymkana) {
    
   // print_object($gymkana);
    
    $gymkana->timecreated = time();

    # May have to add extra stuff in here #
    
    return insert_record("gymkana", $gymkana);
}

/**
 * Given an object containing all the necessary data, 
 * (defined by the form in mod.html) this function 
 * will update an existing instance with new data.
 *
 * @param object $instance An object from the form in mod.html
 * @return boolean Success/Fail
 **/
function gymkana_update_instance($gymkana) {

    $gymkana->timemodified = time();
    $gymkana->id = $gymkana->instance;

    # May have to add extra stuff in here #

    return update_record("gymkana", $gymkana);
}

/**
 * Given an ID of an instance of this module, 
 * this function will permanently delete the instance 
 * and any data that depends on it. 
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 **/
function gymkana_delete_instance($id) {

    if (! $gymkana = get_record("gymkana", "id", "$id")) {
        return false;
    }

    $result = true;

    # Delete any dependent records here #

    if (! delete_records("gymkana", "id", "$gymkana->id")) {
        $result = false;
    }

    return $result;
}

/**
 * Return a small object with summary information about what a 
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @return null
 * @todo Finish documenting this function
 **/
function gymkana_user_outline($course, $user, $mod, $gymkana) {
    return $return;
}

/**
 * Print a detailed representation of what a user has done with 
 * a given particular instance of this module, for user activity reports.
 *
 * @return boolean
 * @todo Finish documenting this function
 **/
function gymkana_user_complete($course, $user, $mod, $gymkana) {
    return true;
}

/**
 * Given a course and a time, this module should find recent activity 
 * that has occurred in gymkana activities and print it out. 
 * Return true if there was output, or false is there was none. 
 *
 * @uses $CFG
 * @return boolean
 * @todo Finish documenting this function
 **/
function gymkana_print_recent_activity($course, $isteacher, $timestart) {
    global $CFG;

    return false;  //  True if anything was printed, otherwise false 
}

/**
 * Function to be run periodically according to the moodle cron
 * This function searches for things that need to be done, such 
 * as sending out mail, toggling flags etc ... 
 *
 * @uses $CFG
 * @return boolean
 * @todo Finish documenting this function
 **/
function gymkana_cron () {
    global $CFG;

    return true;
}

/**
 * Must return an array of grades for a given instance of this module, 
 * indexed by user.  It also returns a maximum allowed grade.
 * 
 * Example:
 *    $return->grades = array of grades;
 *    $return->maxgrade = maximum allowed grade;
 *
 *    return $return;
 *
 * @param int $gymkanaid ID of an instance of this module
 * @return mixed Null or object with an array of grades and with the maximum grade
 **/
function gymkana_grades($gymkanaid) {
   return NULL;
}

/**
 * Must return an array of user records (all data) who are participants
 * for a given instance of gymkana. Must include every user involved
 * in the instance, independient of his role (student, teacher, admin...)
 * See other modules as example.
 *
 * @param int $gymkanaid ID of an instance of this module
 * @return mixed boolean/array of students
 **/
function gymkana_get_participants($gymkanaid) {
    return false;
}

/**
 * This function returns if a scale is being used by one gymkana
 * it it has support for grading and scales. Commented code should be
 * modified if necessary. See forum, glossary or journal modules
 * as reference.
 *
 * @param int $gymkanaid ID of an instance of this module
 * @return mixed
 * @todo Finish documenting this function
 **/
function gymkana_scale_used ($gymkanaid,$scaleid) {
    $return = false;

    //$rec = get_record("gymkana","id","$gymkanaid","scale","-$scaleid");
    //
    //if (!empty($rec)  && !empty($scaleid)) {
    //    $return = true;
    //}
   
    return $return;
}

/**
 * Checks if scale is being used by any instance of gymkana.
 * This function was added in 1.9
 *
 * This is used to find out if scale used anywhere
 * @param $scaleid int
 * @return boolean True if the scale is used by any gymkana
 */
function gymkana_scale_used_anywhere($scaleid) {
    if ($scaleid and record_exists('gymkana', 'grade', -$scaleid)) {
        return true;
    } else {
        return false;
    }
}


/**
 * Agrega una nueva pregunta a la Gymkana.
 *
 * @param $quest object Colección de datos que componen la pregunta a agregar
 * @return int Identificador de la nueva pregnta agregada
 */
function addQuest ($quest) {  
    return insert_record("gymkana_quest", $quest);
}


/**
 * Modifica una nueva pregunta de la Gymkana.
 *
 * @param $quest array Colección de datos que componen la pregunta a actualizar
 * @return boolean True si se actualiza correctamente la pregunta
 */
function updateQuest ($quest) {
    return update_record('gymkana_quest', $quest);
}

/**
 * Elimina una nueva pregunta de la Gymkana.
 *
 * @param $questid int Identificador de la pregunta a eliminar
 * @return boolean True si se elimina correctamente la pregunta
 */
function deleteQuest($questid) {
    return delete_records('gymkana_quest', 'id', $questid);
}



function setGameEndData ($id, $levels) {
    //CALCULO LA NOTA MEDIA OBTENIDA:
    $query = "SELECT SUM(score)  AS score  FROM mdl_gymkana_game_answers WHERE gameid='{$id}'";
    $r = get_record_sql( $query );
    $score = ($r->score/$levels)*100;
    
    

    $quest = new stdClass();
    $quest->id = $id;
    $quest->date = time();
    $quest->score = $score;
    return update_record('gymkana_games', $quest);    

}


function getEmbebdedFile($fileurl) {
    global $CFG, $COURSE;
    $str_file= file_get_contents("{$CFG->dataroot}/{$fileurl}");
    $download_link =  "<a href='{$CFG->wwwroot}/file.php/{$fileurl}'>" . get_string('file2download', 'gymkana') . "</a>";
    
    $type = (empty ($str_file) ) ? null : substr(strrchr($fileurl, '.'), 1);

    if ( empty ($str_file) ) {
        return $download_link;
    } else {
        require_once './lib/geshi/geshi.php';
        $geshi = new GeSHi($str_file);
        $lang = $geshi->get_language_name_from_extension($type);
        if ( empty($lang) ){
            return $download_link; 
        }
        $geshi->set_language( $lang );
        
        $geshi->set_header_type(GESHI_HEADER_DIV);
        
        $geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS );
        $geshi->set_line_style('text-align: left;font-size:11px; background: #CCCCCC;', 'font-size:11px; background: #BBBBBB;', true);
        $geshi->set_tab_width(1);
        
        
        $geshi->set_header_content(substr(strrchr($fileurl, '/'), 1));
        $geshi->set_header_content_style('font-family:Verdana; font-size:14px; color: #ffffff; background: #000000; text-align: center;');
        $geshi->set_footer_content($download_link);
        $geshi->set_footer_content_style('font-family:Verdana; font-size:14px; color: #ffffff; background: #DDDDDD; text-align: center;');
        $geshi->set_comments_style(1, 'font-style: italic;');
        $geshi->set_comments_style('MULTI', 'font-style: italic;');
        return $geshi->parse_code();
    }
}



function getEmbebedSourceCode ($file, $quest_id) {
    global $CFG, $COURSE;
    $uri = "{$COURSE->id}/{$CFG->moddata}/gymkana/questdata/quest_{$quest_id}/{$file}";
    return getEmbebdedFile($uri);
}


function getEmbebedAnswerSourceCode ($file, $gameid, $userid, $level) {
    global $CFG, $COURSE;
    $uri = "{$COURSE->id}/{$CFG->moddata}/gymkana/datausers/{$userid}/game_{$gameid}/level_{$level}/{$file}";
    return getEmbebdedFile($uri);
}


function getPopupInitScript ($selectors) {
    return   '<script type=\'text/javascript\'>
                $(document).ready(function() {
                        $("' . implode(', ', $selectors) . '").fancybox();
                });
              </script>';
}


function getHeader ($title, $navigation, $usefancybox=false) {  
    $navigation = array_values(array_diff($navigation, array('')));
    
    $strnav = implode('-> ', $navigation);
    
    $meta = (!$usefancybox) ? 
    '' : 
    '<script type="text/javascript" src="./lib/jquery/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="./lib/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
    <script type="text/javascript" src="./lib/fancybox/jquery.fancybox-1.3.1.js"></script>
    <link rel="stylesheet" type="text/css" href="./lib/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
    ';
    
    print_header_simple($title, $title, $strnav, '', $meta);
}



function print_graph_data_table ($query) {
    $results = mysql_query($query);
    $num_results = mysql_num_rows($results);    
    if ($num_results > 0) {
        $table->head = array (
                            
                            get_string("idnumber"),
                            get_string("username"),
                            get_string("date"),
                            get_string("hour"),
                            get_string("score","gymkana")
                            
                            );
                            
        for ($i=0; $i<$num_results; $i++) {
            $row = mysql_fetch_array($results);
            $user=get_record("user", "id", $row['user']);
            $table->data[] = array (
                            $i+1,
                            strtoupper("{$user->lastname}, {$user->firstname}") . " ({$user->username})",
                            date('d/m/Y',$row['date']),
                            date('H:i:s',$row['date']),
                            $row['score']
                            );
        }
        $table->align = array ("center", "center", "center", "center", "center");
        print_table($table);
    }
}

?>
