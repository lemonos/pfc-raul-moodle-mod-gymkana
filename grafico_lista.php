<?php

	
	require_once("../../config.php");
	require_once("lib.php");
	include_once("lib/phplot/phplot.php");


//Inicializamos las variables
$cm->id = optional_param('cm->id', 0, PARAM_INT);
    $a  = optional_param('a', 0, PARAM_INT);  // gymkana ID

    if ($cm->id) {
        if (! $cm = get_record("course_modules", "id", $cm->id)) {
            error("Course Module ID was incorrect");
        }
		if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
   
    } 


$name1 = optional_param('nam1', PARAM_ALPHA);
$name2 = optional_param('nam2', PARAM_ALPHA);
$name3 = optional_param('nam3', PARAM_ALPHA);
$name4 = optional_param('nam4', PARAM_ALPHA);
$name5 = optional_param('nam5', PARAM_ALPHA);
$name6 = optional_param('nam6', PARAM_ALPHA);
$name7 = optional_param('nam7', PARAM_ALPHA);
$name8 = optional_param('nam8', PARAM_ALPHA);
$name9 = optional_param('nam9', PARAM_ALPHA);
$name10 = optional_param('nam10', PARAM_ALPHA);


$not1 = optional_param('not1',0, PARAM_INT);
$not2 = optional_param('not2',0, PARAM_INT);
$not3 = optional_param('not3',0, PARAM_INT);
$not4 = optional_param('not4',0, PARAM_INT);
$not5 = optional_param('not5',0, PARAM_INT);
$not6 = optional_param('not6',0, PARAM_INT);
$not7 = optional_param('not7',0, PARAM_INT);
$not8 = optional_param('not8',0, PARAM_INT);
$not9 = optional_param('not9',0, PARAM_INT);
$not10 = optional_param('not10',0, PARAM_INT);

$min = optional_param('min',-10, PARAM_INT);
$max = optional_param('max',10, PARAM_INT);
$numero = optional_param('numero',1, PARAM_INT);
$numnotas = optional_param('numnotas', PARAM_INT);
$ultimo = optional_param('ultimo', PARAM_INT);
$primero = optional_param('primero', PARAM_INT);
$io = optional_param('io', PARAM_INT);

 require_course_login($course);
	  $context = get_context_instance(CONTEXT_MODULE, $cm->id);
	  
$data = array(
  array(($numero+1).': '.$name1.'
  '.$partida1, $not1/100),    array(($numero+2).': '.$name2.'
  '.$partida2, $not2/100),    array(($numero+3).': '.$name3.'
  '.$partida3,  $not3/100),   array(($numero+4).': '.$name4.'
  '.$partida4,  $not4/100),   array(($numero+5).': '.$name5.'
  '.$partida5,  $not5/100),   array(($numero+6).': '.$name6.'
  '.$partida6,  $not6/100),   array(($numero+7).': '.$name7.'
  '.$partida7,  $not7/100),   array(($numero+8).': '.$name8.'
  '.$partida8,  $not8/100),   array(($numero+9).': '.$name9.'
  '.$partida9,  $not9/100),   array(($numero+10).': '.$name10.'
  '.$partida10,  $not10/100));

if ($ultimo==1) 
{
	$primero = $numero-10;
	$ultimo = $numnotas;
}
else
{
	$primero = $numero-10;
	$ultimo = $numero-1;
}
  
$plot = new PHPlot(880, 400);
$plot->SetImageBorderType('plain');

$plot->SetPlotType('bars');
$plot->SetDataType('text-data');
$plot->SetDataValues($data);

# Let's use a new color for these bars:
$plot->SetDataColors('blue');

# Force bottom to Y=0 and set reasonable tick interval:
$plot->SetPlotAreaWorld(NULL,0, NULL,10);
//$plot->SetPlotAreaWorld(NULL,-10, NULL,10);
$plot->SetYTickIncrement(1);
# Format the Y tick labels as numerics to get thousands separators:
$plot->SetYLabelType('data');
$plot->SetPrecisionY(1);

# Main plot title:
$plot->SetTitle('Resultados del '.($numero+1). ' al '.($numero+10));
# Y Axis title:
$plot->SetYTitle('Nota');

# Turn off X tick labels and ticks because they don't apply here:
$plot->SetXTickLabelPos('none');
$plot->SetXTickPos('none');

$plot->DrawGraph();




?>