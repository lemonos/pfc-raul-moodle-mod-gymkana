<?php

	require_once("../../config.php");
	require_once("lib.php");
	
	$cm_id = optional_param('cmid', 0, PARAM_INT);
    $action = optional_param('action','userslist',PARAM_TEXT);
    $userid = optional_param('uid', 0, PARAM_INT);
    $gameid = optional_param('gid', 0, PARAM_INT);

    if ($cm_id) {
        if (! $cm = get_record("course_modules", "id", $cm_id)) {
            error("Course Module ID was incorrect");
        }
		if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
   
    } 
		 
    require_course_login($course);
    $context = get_context_instance( CONTEXT_MODULE, $cm->id );
  
    //muestro el fichero
    if ($action == 'showfile' && !empty($userid) && !empty($gameid) ) {
        print_header();
        print_simple_box_start();

        $q = get_record("gymkana_game_answers", "id", $gameid);
        echo getEmbebedAnswerSourceCode ($q->ansupload, $q->gameid, $userid, $q->levelquest);
        
        print_simple_box_end();
        print_footer('none');
        die();
    }

    add_to_log($course->id, "gymkana", "view", "resultado_alumnos.php?id=".$cm->id, $gymkana->id);
	
/// Print the page header
    $strgymkanas = get_string("modulenameplural", "gymkana");
    $strgymkana  = get_string("modulename", "gymkana");
	
    
    $meta = '<script type="text/javascript" src="./lib/jquery/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="./lib/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
    <script type="text/javascript" src="./lib/fancybox/jquery.fancybox-1.3.1.js"></script>
    <link rel="stylesheet" type="text/css" href="./lib/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
    ';
    
	$navigation1 = "<a href=\"index.php?id=$course->id\">$strgymkanas</a> ->";			  
	$navigation2 = "<a href=\"view.php?a=$gymkana->id\">".stripslashes($gymkana->name)."</a> -> ";
	print_header_simple($gymkananame." - Resultados alumnos", "",
                 "$navigation1 "."$navigation2 "."Resultados alumnos", "", $meta, true, "");


    //INTRODUZCO PESTAÑAS
    $currenttab = 'results';
    include ('tabs.php');     
    
   // DebugBreak();
    switch ($action){
        case "userslist":
            $query = "SELECT user, COUNT(user)  AS games  FROM mdl_gymkana_games WHERE gymkana='{$gymkana->id}' GROUP BY user";
            $users = get_records_sql( $query ); 
         
            if (!empty($users) && count($users) >0 ) {
                print_simple_box_start ("center");
                $table->head = array (
                                        get_string("username"), 
                                        get_string("games", "gymkana"),
                                        );
                foreach ($users as $u) {
                    
                    $user = get_record('user', 'id', $u->user);
                    $table->data[] = array (
                                        strtoupper("{$user->lastname}, {$user->firstname}") . " ({$user->username})", 
                                         "<a href='resultado.php?cmid={$cm->id}&uid={$user->id}&action=usergames'>{$u->games}</a>"
                                        );
                }
                
                $table->align = array ("left", "center");

                print_table($table);
                print_simple_box_end();
              
          } else {
              print_simple_box_start ("center");
              echo get_string("not_found_users", "gymkana");
              print_simple_box_end();
          }
            

            break;
            
        case "usergames":
            $usergames = get_records_select('gymkana_games', "user={$userid} AND gymkana={$gymkana->id}" );     
            $user = get_record('user', 'id', $userid);
            print_simple_box_start ("center");
            print_heading( strtoupper("{$user->lastname}, {$user->firstname}") . " ({$user->username})" );
            
            $table->head = array (
                                    get_string("idnumber"),
                                    get_string("initdate", "gymkana"), 
                                    get_string("enddate", "gymkana"),
                                    get_string("score", "gymkana"),
                                    ''
                                    );
            foreach ($usergames as $game) {
                $table->data[] = array (
                                    $game->id,
                                    userdate ( $game->initdate ),
                                    userdate( $game->date ),
                                    $game->score,
                                     "<a href='resultado.php?cmid={$cm->id}&uid={$user->id}&gid={$game->id}&action=gameresults'>" . get_string("view") . "</a>"
                                    );
            }
            
            $table->align = array ("center", "left", "left", "center");

            print_table($table);
            print_simple_box_end();
            break;
            
        case "gameresults":
            $gameresults = get_records_select('gymkana_game_answers', "user={$userid} AND gameid={$gameid}", "id ASC" ); 
   
            $user = get_record('user', 'id', $userid);
            $p= new stdClass();
            $p->gameid = $gameid;
            $p->user = strtoupper("{$user->lastname}, {$user->firstname}") . " ({$user->username})";
            print_simple_box_start ("center");
            print_heading( get_string("gameresult_head", "gymkana", $p) );
     
            $table->head = array (
                                    get_string("level", "gymkana"), 
                                    get_string("idnumber"),
                                    get_string("answer", "gymkana"),
                                    get_string("answerfile", "gymkana"),
                                    get_string("score",  "gymkana"),
                                    get_string("date"),
                                    get_string("ip_address"));
                                    
            
                       
            $fancyboxselector = array();      
            
            foreach ($gameresults as $result) {
                $fancyboxselector[] = "#sourcemodal_{$result->id}";
                $path = ( !empty($result->ansupload) ) ? "resultado.php?cmid={$cm->id}&uid={$userid}&gid={$result->id}&action=showfile" : "";
                $table->data[] = array (
                                    $result->levelquest,
                                    $result->idquest,
                                    purify_html($result->answer),
                                     ( empty($path) ) ? "" : "<a id='sourcemodal_{$result->id}' href='{$path}'>{$result->ansupload}</a>",
                                     $result->score,
                                     userdate($result->date),
                                     $result->ipuser
                                    );
            }
            
            $fancyboxscript .= '});</script>';
            
            
            $table->align = array ("center", "center", "left", "left", "center", "left", "center");

            print_table($table);

            echo ( getPopupInitScript($fancyboxselector) ); 
            print_simple_box_end();
            break;
        
    }        
                 
                 

/// Finish the page
	print_footer($course);

?>