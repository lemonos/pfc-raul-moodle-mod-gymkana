<?php

$string['gymkana'] = 'gymkana';

$string['modulename'] = 'Gymkana';
$string['modulenameplural'] = 'Gymkanas';

$string['begin_game'] = 'Comenzar el Juego';
$string['games'] = 'Juegos';
$string['gameresult_head'] = 'Resultados del Juego $a->gameid para $a->user';

$string['allowstudentuplevel'] = 'Permitir al alumno subir de nivel en caso de fallar la pregunta';
$string['allowrepeatquest'] = 'Permitir repetir preguntas ya realizadas por el alumno en el juego';

$string['userreports'] = 'Mis Resultados';
$string['quests'] = 'Retos';
$string['managequest'] = 'Gestionar Retos';
$string['addquest'] = 'Añadir Reto';
$string['editquests'] = 'Modificar Retos';
$string['reports'] = 'Informes';
$string['results'] = 'Resultados';
$string['graphics'] = 'Gráficos';
$string['statistics'] = 'Estadísticas';

$string['level'] = 'Nivel';
$string['quest'] = 'Enunciado';
$string['answer'] = 'Solución';
$string['help'] = 'Ayuda';
$string['shortquest'] = 'Pregunta';
$string['shortanswer'] = 'Respuesta';
$string['action'] = 'Acción';
$string['studentanswer'] = 'Responde para ver el nivel en que deberías continuar';

$string['not_found_quests'] ='No se han encontrado retos';
$string['not_found_users'] ='No se han encontrado usuarios que hayan realizado la Gymkana';
$string['quest_delete'] ='Se ha eliminado el reto';
$string['error_quest_delete'] ='Se ha producido un error en el proceso de borrado del reto';
$string['error_quest_update'] = 'Se ha producido un error en el proceso de actualización del reto';

$string['intro'] = 'Introducción';
$string['timequest'] = 'Tiempo para cada reto';
$string['numberlevels'] = 'Número de Niveles';

$string['mostrarindice'] = 'Mostrar Indice';

$string['write_answer'] = 'Escribir la respuesta al programa.';

$string['file_upload_error'] = 'Error subiendo el fichero ($a->filename) al directorio $a->uploaddir';
$string['new_quest'] = 'Introducir nuevo Reto';
$string['enter_quest_result'] = 'Se ha creado el reto con las siguientes datos: ';
$string['other_quest'] = 'Introducir otro Reto';
$string['file2upload'] = 'Programa a desarrollar';
$string['uploadfiledevelop'] = 'Subir Programa Desarrollado';
$string['file2download'] = 'Descargar el fichero';
$string['current_file'] = 'Fichero Actual';
$string['select_level'] = 'Selecciona Nivel...';
$string['choose_one'] = 'Elige una de las dos opciones';
$string['quest_update'] = 'Reto Actualizado';

$string['initdate'] = 'Fecha de Inicio';
$string['enddate'] = 'Fecha de Fin';
$string['score'] = 'Puntuación';
$string['answerfile'] = 'Archivo de Respuesta';

$string['gameend'] = 'El juego ha finalizado';
$string['newgame'] = 'Iniciar nuevo juego';
$string['maxtimeexceeded'] = 'Has superado el tiempo máximo pérmitido para el juego';
$string['timereport'] = '<p>Tiempo utilizado: $a->timespent segundos</p><p>Tiempo máximo permitido: $a->timequest segundos</p>';
$string['correctanswer'] = 'La respuesta a la pregunta realizada es correcta';
$string['incorrectanswer'] = 'La respuesta a la pregunta realizada es incorrecta';
$string['yoursolution'] = 'Tu solución';
$string['repeatlevel'] = 'Repetir Nivel';
$string['nextlevel'] = 'Siguiente Nivel';
$string['uplevelrecomendation'] = 'En base a la respuesta que has dado te recomendamos subir de Nivel.';
$string['samelevelrecomendation'] = 'En base a la respuesta que has dado te recomendamos mantenerte en el mismo nivel.';
$string['codeanalysistext'] = 'Compara el código de tu programa con la solución que te planteamos y decide por ti mismo si estás preparado para el siguiente nivel, o por el contrario prefieres realizar otro reto del nivel actual';

$string['selecttypestatistics'] = 'Seleccione el tipo de estad&iacute;stica a mostrar';
$string['perusers'] = 'Por Usuarios';
$string['perlevels'] = 'Por Niveles';
$string['participants'] = 'Usuarios participantes en esta Gymkana';
$string['userselected'] = 'Usuario seleccionado: ';
$string['answerid'] = 'ID Pregunta';
$string['answeroccurrences'] = 'Ocurrencias';
$string['successes'] = 'Aciertos';
$string['failures'] = 'Fallos';
$string['wrongpercent'] = 'Porcentaje de Fallo';
$string['succespercent'] = 'Porcentaje de Acierto: ';
$string['not_found_record'] = 'No se han encontrado registros';
$string['levels'] = 'Niveles de Juego';
$string['levelselected'] = 'Nivel seleccionado: ';
$string['mystatistics'] = 'Mis Estadísticas';
$string['allresults'] = 'Todos los resultados';
?>
