<?php

$string['gymkana'] = 'gymkana';

$string['modulename'] = 'Gymkana';
$string['modulenameplural'] = 'Gymkanas';

$string['begin_game'] = 'Begin Game';
$string['games'] = 'Games';
$string['gameresult_head'] = 'Results of Game $a->gameid for $a->user';

$string['allowstudentuplevel'] = 'Permitir al alumno subir de nivel en caso de fallar la pregunta';
$string['allowrepeatquest'] = 'Permitir repetir preguntas ya realizadas por el alumno en el juego';

$string['userreports'] = 'My Reports';
$string['quests'] = 'Quests';
$string['managequest'] = 'Manage Quests';
$string['addquest'] = 'Add new quest';
$string['editquests'] = 'Edit current quests';
$string['reports'] = 'Reports';
$string['results'] = 'Results';
$string['graphics'] = 'Graphics';
$string['statistics'] = 'Statistics';

$string['level'] = 'Level';
$string['quest'] = 'Statement';
$string['answer'] = 'Solution';
$string['help'] = 'Help';
$string['shortquest'] = 'Question';
$string['shortanswer'] = 'Answer';
$string['action'] = 'Action';
$string['studentanswer'] = 'Answer to see the level at which you should continue';

$string['not_found_quests'] ='Not found any quest';
$string['not_found_users'] ='No users found to have made the Gymkhana';
$string['quest_delete'] ='Quest deleted';
$string['error_quest_delete'] = 'There was an error in the process of deleting the quest';
$string['error_quest_update'] = 'There was an error in the process of updating the quest';


$string['intro'] = 'Introduction';
$string['timequest'] = 'Tiempo for each answer';
$string['numberlevels'] = 'Number of levels';

$string['mostrarindice'] = 'Show Index';

$string['write_answer'] = 'Write the Answer.';

$string['file_upload_error'] = 'Error upload file  ($a->filename) to folder $a->uploaddir';
$string['new_quest'] = 'Enter a new Quest';
$string['enter_quest_result'] = 'Quest has been created with the following information:';
$string['other_quest'] = 'Enter other Quest';
$string['file2upload'] = 'Program to Develop';
$string['uploadfiledevelop'] = 'Upload a File like Answer to the Program to be Developed';
$string['file2download'] = 'Download the file';
$string['current_file'] = 'Current File';
$string['select_level'] = 'Select the Level...';
$string['choose_one'] = 'Choose One';
$string['quest_update'] = 'Quest Updated';

$string['initdate'] = 'Init Date';
$string['enddate'] = 'End Date';
$string['score'] = 'Score';
$string['answerfile'] = 'Answer File';


$string['gameend'] = 'The game is finished';
$string['newgame'] = 'Start new game';
$string['maxtimeexceeded'] = 'You\'ve exceeded the maximum time allowed for the game';
$string['timereport'] = '<p> Time used: $a->timespent seconds </ p> Maximum time allowed: $a->timequest seconds </ p>';
$string['correctanswer'] = 'The answer to the question asked is correct';
$string['incorrectanswer'] = 'The answer to the question asked is incorrect';
$string['yoursolution'] = 'Your solution';
$string['repeatlevel'] = 'Repeat Level';
$string['nextlevel'] = 'Next Level';
$string['uplevelrecomendation'] = 'Based on the answer you have given we recommend raising the level';
$string['samelevelrecomendation'] = 'Based on the answer you have given we recommend staying at the same level';
$string['codeanalysistext'] = 'Compare the code of your program with the solution you propose and decide for yourself if you are prepared to the next level, or instead prefer to make another challenge the current level.';

$string['selecttypestatistics'] = 'Select the type of statistics to show';
$string['perusers'] = 'Per Users';
$string['perlevels'] = 'Per Levels';
$string['participants'] = 'Users participating in this Gymkhana';
$string['userselected'] = 'User selected: ';
$string['answerid'] = 'ID Question';
$string['answeroccurrences'] = 'Occurrences';
$string['successes'] = 'Hits';
$string['failures'] = 'Failure';
$string['wrongpercent'] = 'Failure Rate';
$string['succespercent'] = 'Success Rate: ';
$string['not_found_record'] = 'No records found';
$string['levels'] = 'Levels of Game';
$string['levelselected'] = 'Selected Level: ';
$string['mystatistics'] = 'My Statistics';
$string['allresults'] = 'All Results';




?>
