<?php
	
	require_once("../../config.php");
	require_once("lib.php");

 
    $cm_id = optional_param('cmid', 0, PARAM_INT);  
    $accion =  optional_param('action', 'default', PARAM_ALPHA);
    $questid = optional_param('qid', 0, PARAM_INT);
    
    if (!empty($cm_id) ) {
        if (! $cm = get_record("course_modules", "id", $cm_id)) {
            error("Course Module ID was incorrect");
        }
		if (! $course = get_record("course", "id", $cm->course)) {
            error("Course is misconfigured");
        }

        if (! $gymkana = get_record("gymkana", "id", $cm->instance)) {
            error("Course module is incorrect");
        }
   
    }
    
    
  
  require_course_login($course);
  $context = get_context_instance(CONTEXT_MODULE, $cm->id);

  //muestro el fichero
    if ($accion == 'showfile' && !empty($questid) ) {
        print_header();
        print_simple_box_start();
        
        $q = get_record("gymkana_quest", "id", $questid);
        echo getEmbebedSourceCode ($q->file, $q->id);
        
        print_simple_box_end();
        print_footer('none');
        die();
    }
  
  if ( !empty($questid) ) {
      if (! $quest = get_record("gymkana_quest", "id", $questid)) {
            error("Quest ID was incorrect");
        }     
  }

    add_to_log($course->id, "gymkana", "view", "ansfile.php?id=".$cm->id, $gymkana->id);
	
	$navigation = array ();
    $navigation[] = "<a href=\"view.php?a=$gymkana->id\">".$gymkana->name."</a>";
    $navigation[] = get_string ('managequest', 'gymkana');
                 
    getHeader(format_string($gymkana->name), $navigation, true);
                 
    //INTRODUZCO PESTAÑAS
    $currenttab = 'managequest';
    include ('tabs.php'); 

    

switch ($accion) {
     case 'edit':
        $quest = get_record("gymkana_quest  ", "id", $questid);
     
        require_once ('./manage_quest_form.php');
        
        $customdata = array();
        $customdata['maxlevels'] = $gymkana->levels;
        $customdata['moduleid'] = $cm_id;
        $customdata['questid'] = $quest->id;
        $customdata['file'] = $quest->file;
        $edit_quest_form = new edit_quest_form(null, $customdata);
  
        $edit_quest_form->set_data($quest);
        
        if ( $updatequest = $edit_quest_form->get_data()) { 
           
            $newfilename = $edit_quest_form->get_new_filename();
            
            if (!empty($newfilename) || (isset($updatequest->deletefile) && $updatequest->deletefile) ) {
                $location = $path2modata = "$CFG->dataroot/{$course->id}/{$CFG->moddata}/gymkana/questdata/quest_{$quest->id}"; 
                @remove_dir($location);
                $updatequest->file = '';
                $updatequest->ans = '';
            } 

            if ( !empty( $newfilename ) ) {
                $path2modata = "{$course->id}/{$CFG->moddata}/gymkana/questdata/quest_{$quest->id}";
                $upload_dir = make_upload_directory($path2modata);
 
                //Subo el fichero al directorio creado
                if (!$edit_quest_form->save_files($upload_dir)) {
                    $a=new object();
                    $a->filename  = $edit_quest_form->get_new_filename();
                    $a->uploaddir = $upload_dir;
                    notify(get_string('file_upload_error','gymkana', $a),'admin');   
                }  else {
                    $updatequest->file = $newfilename;
                    $updatequest->ans = '';
                    //
                }               
                
            }
            
            
            if ( updateQuest ( $updatequest ) ) {
                redirect("{$CFG->wwwroot}/mod/gymkana/manage_quest.php?cmid={$cm_id}", get_string("quest_update", "gymkana"), 1); 
            } else {
                redirect("{$CFG->wwwroot}/mod/gymkana/manage_quest.php?cmid={$cm_id}", get_string("error_quest_update", "gymkana"), 1); 
            }
        }
        $edit_quest_form->display();
        
        break;
          
     case 'delete':
        $quest = get_record("gymkana_quest  ", "id", $questid);
        print_simple_box_start ("center");
       
        if ( !empty($quest)  ) {
            if ( !empty($quest->file) ) {
                $quest->ans = getEmbebedSourceCode( $quest->file, $quest->id );
            }
            $table->head = array (
                                    get_string("level", "gymkana"), 
                                    get_string("quest", "gymkana"),  
                                    get_string("answer", "gymkana"),
                                    get_string("help", "gymkana"),
                                    get_string("shortquest", "gymkana"),
                                    get_string("shortanswer", "gymkana"),
                                    ''
                                    );
            
            $table->data[] = array (
                                    stripslashes($quest->level), 
                                    purify_html($quest->quest), 
                                    purify_html($quest->ans), 
                                    purify_html($quest->help), 
                                    purify_html($quest->shortquest), 
                                    purify_html($quest->shortans),
                                    "<a href='{$CFG->wwwroot}/mod/gymkana/manage_quest.php?cmid={$cm->id}&qid={$questid}&action=deleteOK'>" . get_string('confirm') . "</a><br><a href='{$CFG->wwwroot}/mod/gymkana/manage_quest.php?cmid={$cm->id}'>" . get_string('cancel') . "</a>"
                                    );
            
            $table->align = array ("left", "left", "left", "left", "left", "left","center");

            print_table($table);     
            
            print_simple_box_end(); 
              
          } else {
              print_simple_box_start ("center");
              echo get_string("not_found_quests", "gymkana");
              print_simple_box_end();
          }
        break;
        
     case 'deleteOK':
          
        if ( deleteQuest( $questid ) ) {
            redirect("{$CFG->wwwroot}/mod/gymkana/manage_quest.php?cmid={$cm_id}", get_string("quest_delete", "gymkana"), 1); 
        } else {
            redirect("{$CFG->wwwroot}/mod/gymkana/manage_quest.php?cmid={$cm_id}", get_string("error_quest_delete", "gymkana"), 1); 
        }
        
     default:

          $quests = get_records("gymkana_quest  ", "gymkana", $gymkana->id, 'level ASC, id ASC');
        
       
          if (!empty($quests) && count($quests) >0 ) {
           
            print_simple_box_start ("center");  
            
            $table->head = array (
                                    get_string("level", "gymkana"), 
                                    get_string("quest", "gymkana"),  
                                    get_string("answer", "gymkana"),
                                    get_string("help", "gymkana"),
                                    get_string("shortquest", "gymkana"),
                                    get_string("shortanswer", "gymkana"),
                                    ''
                                    );
            $fancyboxselector = array();                                  
            foreach ($quests as $q) {
                if ( !empty($q->file) ) {
                    $fancyboxselector[] = "#sourcemodal_{$q->id}";
                    $path = "manage_quest.php?cmid={$cm->id}&qid={$q->id}&action=showfile";
                    $q->ans = "<a id='sourcemodal_{$q->id}' href='{$path}'>{$q->file}</a>";
                }
                $table->data[] = array (
                                    stripslashes($q->level), 
                                    purify_html($q->quest), 
                                    ( !empty($q->file)) ? $q->ans :purify_html($q->ans), 
                                    purify_html($q->help), 
                                    purify_html($q->shortquest), 
                                    purify_html($q->shortans), 
                                    "<a href='manage_quest.php?cmid={$cm->id}&qid={$q->id}&action=edit'>" . get_string("edit") . "</a><br><a href='manage_quest.php?cmid={$cm->id}&qid={$q->id}&action=delete'>" . get_string("delete") . "</a>"
                                    );
            }
            
            $table->align = array ("left", "left", "left", "left", "left", "left", "center");
            
            print_table($table);
            echo ( getPopupInitScript($fancyboxselector) ); 
            print_simple_box_end(); 
          } else {
              print_simple_box_start ("center");
              echo get_string("not_found_quests", "gymkana");
              print_simple_box_end();
          }
          


            
          
          
          
          print_simple_box_end();
    
        break;
}

	/// Finish the page
	print_footer($course);

?>
    